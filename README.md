Andrei's PhD Thesis - in progress

Thesis title:
Towards an optimal serverless runtime for stateful data analytics on hybrid edge-cloud infrastructures

Supervisors:
Grégory Mounié and Denis Trystram, Grenoble INP, LIG
Yiannis Georgiou and Michael Mercier, Ryax Technologies
