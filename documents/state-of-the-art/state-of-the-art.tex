\section{General context}

Serverless Computing has emerged as a new paradigm of abstraction, platform and implementation of cloud functions~\cite{barcelona-pons_faas_2019, baldini_serverless_2017}. It has been seen as an evolution of the Cloud Computing model in the sense of use of micro services and containers.
%
From the perspective of IaaS - Infrastructure as a Service - it can benefit both, developers and cloud providers. 
From the developers point of view it offers a simplified programming model, abstracting the operational concerns. In addition, it is expected to be cheaper due to a billing model based on the functions execution time instead of the resource allocation.
In this context, it is possible to deploy rapidly small functions which respond to events. Such events can be coordinated to behave as micro services, which would be done before by dedicated middle-wares.
From the cloud provider point of view, they benefit having control of the full stack and then can reduce costs controlling the resources. Although, they also need to provide mechanisms such as scaling, fault-tolerance and monitoring, which became a challenge.
%
Different from PaaS - Platform as a Service - that also abstract the management of servers, serverless models consist in stateless functions that can be developed without depending on any specific prepacked application. This development is characterized as FaaS - Functions as a Service.
The FaaS concept was first presented by AWS in 2014 with Lambda~\cite{awslambda}. After that, in 2016, other vendors as Google, Microsoft and IBM followed AWS introducing Google Cloud Functions~\cite{cloudfunctions}, 
Microsoft Azure Functions~\cite{azurefunctions} and IBM Cloud Function~\cite{ibmcloudfunctions}.

The two main concepts that define Serverless Computing are the billing and the programming model. The billing model is based on the functions execution time. This way it can follow the capability of serverless platforms to scale up and down automatically. Named scalability, it means that an instance can get more resources when needed, as well as scale down to zero when an it becomes idle. This avoid to keep resources reserved waiting for calls of the same function. In addition to reduce costs, when a function is scaled down it releases resources and addresses the problem of resources limitation. The last happens when it is needed to scale up some instance but there are any resource available.
%
The programming model of Serverless Computing is based on descriptive files such as JSON or YAML. Only a main function should be executed, in general, receiving such files as input and producing some others as outputs. Based on that, there are some mechanisms to compose functions. The deployment should be simple, only providing the files with the source code, or packing them into containers such as Docker Images.

Regarding security, serverless platform are multi-tenant, so all executions should be isolated. In addition, the use of resource should be managed separated to bill correctly each user.
The control and management of errors are also important concepts. Since users do not know where their jobs are executed, it is not possible to access those machines and follow the functions execution or debugging them. So, logs of errors are provided as small mechanisms for that.

The Serverless architecture is an event driven processing system. There are a set of functions defined by the developer who will invoke them by events over HTTP calls. Once invoked, an instance of the function should be deployed and executed in the platform. When it is no longer needed, it should be stopped and removed from the platform.
%
The challenges comes out when the platform should respect this architecture and consider metrics such as cost, scalability and fault tolerance. The platform must start a function quickly when it is called, managing a queue of events and scheduling the instance of the function among the available resources. The platform should know when to stop and deallocate a function when it becomes idle. In addition it should know when to scale up or down the resources allocated to such function, as well as provide fault tolerance mechanisms.

\section{Challenges and Open Tasks}

There are several challenges presented in the literature and they are generally grouped, for instance, as system or programming model levels, as pointed in a global view by Baldini et al.~\cite{baldini_serverless_2017}. Other groups are proposed by Jonas et al.~\cite{jonas_cloud_2019}, being more specific such as abstract, network, security and architecture. Beside different kind of classification, the raised challenges are common among the works. 
%
The cost is one of the most important challenges, which is intended to be minimized by a better resource management. For that, we find other challenges nested to it as scalability and functions cold start latency.
The resource management is very important to achieve the best use of the platform. It addresses to the scalability and the platform provider is responsible for both. But, they become more difficult considering that there is no or few application-level knowledge.
By one side scale to zero is very useful when functions are idle and address to the challenge of resources limitation, but for the other side it turns to increase functions cold start latency that could be avoided by keeping resources warm.
%
Serverless Platform can be composed by hybrid components and it should handle different kind of applications with different characteristics. Reinforcing the importance of management of resources, such different applications can be deployed in the same machine, which is known as multi-tenant hosting. Such approach requires strong isolation and security techniques to avoid the lacking of sensitive data, even among the instances in the same machine or among the network.

Focusing on the programming model and dev-ops challenges, the traditional tools should not work - here we consider the ones for general Cloud Computing propositions - for instance, on serverless we use a declarative approach for deployment, which facilitates the process from the development point of view.
In addition, it address to the challenge of composability, helping the developer to describe a flow of functions. 
Besides, it is still not clear how to deal with states among them in the serverless context.
Monitoring and tracking function are still other challenges. Since users have no access to the servers detail, the only information they know comes from their functions execution and few other details provided by the platform. Regarding the functions execution time, serverless nature can imply in short running functions. Then it should provide a way to deal with long running jobs, if by splitting them into small jobs, it should keep tracking of the logs as a long job.

\section{Investigating Serverless Platforms and Scheduling Policies}

%\section{Stateful Serverless Platforms}

Several platforms have emerged into the literature aiming to address the challenges presented above.
Considering data management, for example, due to the stateless nature of serverless computing, its very difficult to manage it. Klimovic et al.  present Pocket that provides its efficiency by a design of an elastic, fully managed cloud storage service~\cite{klimovic_elastic_2019}.
Wu et al. addressed to the same challenge, with Anna, a data management system based on key-value storage system~\cite{wu_anna_nodate}.

Considering the attempts in regard to the design of stateful applications, Sreekanti et al. propose Cloudburst, which is a stateful FaaS platform that provides a solution for communicating and sharing mutable states among functions with low-latency while maintaining the serverless constraints~\cite{sreekanti_cloudburst_2020}. 
In addition, Cloudburst uses Anna~\cite{wu_anna_nodate} for such sharing of states.
Akkus et al. propose SAND, it is another serverless platform that address to the same challenge. It is based on an hierarchical message queuing and storage mechanism~\cite{akkus_sand_nodate}.
Barcelona-Pons et al. designed a system for highly-concurrent stateful applications, named Crucial. It allows the port multi-threaded algorithms to a serverless environment~\cite{barcelona-pons_faas_2019}.
Akhter et al. do not present a tool, but propose a model for developing stateful functions and deploying them. Based on data-flow graphs this model allows functions to retain state as well as call other functions~\cite{akhter_stateful_2019}.

In the remaining of this Section we will construct the concepts to approach the scheduling of functions upon Serverless Platforms. It will follow discussing in more details about some of them and how to deploy its functions in Section~\ref{subsec:deployment}; some details about Kubernetes - a container orchestration platform that has been used for Serverless goals - as well as its scheduler in Section~\ref{subsec:kubernetes_openwhisk} and Section~\ref{subsec:kubernetes_scheduler} respectively. We will present scheduling algorithms and solutions found in the state of the art in Section~\ref{subsec:scheduling}; then we will approach simulations as a way to investigate scheduling policies, in the level of general platforms in Section~\ref{subsec:simulations} and in the Global Continuum of Cloud and Edge Platforms in Section~\ref{subsec:global_continuum}.

\subsection{Deployment of Functions on Serverless Platforms}
\label{subsec:deployment}

% Talking about servelrs
Serverless Computing has emerged as a new paradigm of abstraction, platform and implementation of cloud functions. It has been seen as an evolution of the Cloud Computing model in the sense of use of micro services and containers.
Containers look to be one of the best options to work with isolated and controlled environments.
With them it is possible to pack a whole environment and to deploy it anywhere. Within such environment,
of course, it is added the application which will be executed when such container would be deployed.
For instance, an application can be an entire program, or a part of that - such as a set of functions
that can run separated but coordinated to join the main program in the end - or even small functions
that do not communicate to any other.
From the first scenario we could say that we are talking about a monolithic architecture application,
from the second one, a micro-services architecture application and from the third one we are talking about function as a service modeled applications.
Please, notice that we are not discussing the efficiency of these approaches, the focus is to emphasize that
containers can pack applications for several proposes and its environment. Once pointed its importance, it is needed something to manage them.
Kubernetes is one of the options for that. It provides a container management upon platforms. It is possible
to deploy, schedule, execute them and so on.

\subsection{Serverless Platforms and Kubernetes}
\label{subsec:kubernetes_openwhisk}

As mentioned above, FaaS applications can benefit from containerization, so they also can be managed by Kubernetes based platforms.
Serverless Computing evolves the FaaS concept avoiding the server infrastructure management. So, if by one side
FaaS applications benefit from Kubernetes based platforms, from the other side, they still handle the servers infrastructure.
At this point, there were developed several Serverless Platforms on top of Kubernetes such as Kubeless~\cite{kubeless}, %OpenShift~\cite{open_shift_kubernetes}, its more like a production version of Kubernetes
OpenWhisk~\cite{openwhisk} and OpenFaas~\cite{openfaas}.
These platforms handle automatically the Kubernetes configuration side to make easy to the developers to upload, deploy and execute their functions.
%So, Kubernetes is not a Serverless solution by itself. It is a container management platform, which fits in the scenario
%of FaaS implementation by containers. In addition, other frameworks automated all the management process to deploy such functions to such platforms, facilitating the platform management from the developer point of view. 
For that, they create all the services and operators needed as Pods within such Clusters. When an action is needed to be done they use these Pods. 
For instance, when OpenWhisk is deployed upon a Kubernetes cluster, it creates Pods to manage its administration, alarms, gateways, schedulers and so on. It also creates Pods to connect to external services - with which it was developed - such as Kafka, NGINX and CouchDB. 
Similarly, OpenFaas creates its own Pods for helping its management.

Going into more details of OpenWhisk, it abstracts a function as an \textit{Action}, which is called by a \textit{Trigger} and follows a \textit{Rule}. Developers can deploy their functions and define such triggers and rules. With all of that they also can combine actions - functions - with \textit{Events}.
OpenWhisk architecture is based on \textit{a) a NGINX mechanism}, \textit{b) a CouchDB database}, \textit{c) a Controller}, \textit{d) a Kafka messaging mechanism} and \textit{e) Invokers}. 
The NGINX mechanism handles HTTPs requests; the CouchDB mechanism saves the platform information such as the Actions defined; the Controller  decides what to do with the HTTP requests received and with the information saved on the CouchDB. In addition it sends Actions to be executed in the Invokers; the Kafka mechanism manages the messages among the Controller and the Invokers; and finally, the Invokers execute the Actions within containers.

\subsubsection{Kubernetes Standard Scheduler}
\label{subsec:kubernetes_scheduler}

% Scheduling Algortihms for FaaS

% To merge 4.2 and 4.3
% To reverse the order: first the academic side, then the details of Kubernetes
The decisions regarding to where to execute Pods are taken by schedulers.
Kube-scheduler is the default scheduler of Kubernetes and its role is to dispatch Pods to Nodes based on a scheduling policy~\cite{kubernetes_scheduler}.
Depending on the metric focused by the platform, sometime the best allocation is not achieved with such scheduler.
Besides Kubernetes allows the developers to modify its default one, there are some available
options such other open source schedulers for Kubernetes such as Volcano~\cite{volcano_scheduler}, YuniKorn~\cite{yunikorn,incubator_yunikorn_core}, Safe Scheduler~\cite{ibm_safe_scheduler} and Multiple Cluster Dispatcher~\cite{ibm_multi_cluster_dispatcher}.
Each one address to some of the different focus on scheduling objectives.

Since all the tools mentioned above are based on Kubernetes and its default scheduler, we will depict its mechanism. The Kubernetes scheduling process is based in some steps where filtering and scoring are the two main ones.
Basically, before scheduling a Pod to a Node, the kube-scheduler filters all
available Nodes and if there is any, it scores them to select the most valuable one.
If kube-scheduler algorithm is not the most suitable option for some specific case,
Kubernetes allows developers to change it.
Policies~\cite{kubernetes_policies} are the way that Kubernetes implements the filtering and scoring phases
for the scheduler. Anyone can access and change its parameters.

Furthermore, Kubernetes split its scheduling process by Stages.
For instance, QueueSort, Filter, Score, Bind, Reserve and others.
Each Stage represents a scheduling step and they are exposed by Extension Points.
Extension Points behaviors are implemented by Plugins~\cite{kubernetes_plugins}.
A Plugin can be used by one or more Extension Points.
In the end, the set of Extension Points and its Plugins compose a Profile~\cite{kubernetes_profiles}.
A Profile is a mechanism that allows developers to configure Plugins to implement different scheduling behaviors for different Stages.
If not provided when a Pod is created, Kubernetes considers its default Profile, the default-scheduler.
There is also the possibility to develop several Profiles and to use them within different Pods as a multiple-scheduler mechanism~\cite{kubernetes_multiple_scheduler}.
For instance, an example is the QueueSort Extension Point. As an Extension Point it defines a scheduling Stage.
It provides ordering functions to sort the Pods in the schedule queue.
To do so the QueueSort Extension Point uses the PrioritySort Plugin, that
implements the default priority based sorting.
With many others, QueuSort Extension Point, and consequentely the PrioritySort Plugin, compose the default-scheduler Profile.

Since Kubernetes environment evolves and changes over time, some resources might become under or over usage. For instance, a very simple example is if a Pod fails and it is duplicated for fault tolerant reasons. If the failed Pod becomes
available again the Cluster would be running two instances of the same Pod.
For this and other reasons, there is a mechanism to avoid such under or over resources usage. Named descheduler~\cite{kubernetes_descheduler}, it goal is to find pods that can be moved and evict them. This not mean that descheduler replace the evicted pods, if needed it will be done by the scheduler itself.

\subsection{Scheduling Algorithms for FaaS}
\label{subsec:scheduling}

Scheduling algorithms can be used for several reasons, such as to minimize functions response time, to save costs, to reduce data movements, or energy consumption and so on.  
Addressing to the problem of functions requirement locality, Aumala et al. proposed a scheduling policy based on the packages needed to execute a function~\cite{aumala_beyond_2019}. The package-aware scheduling proposed, PASch, considers the package affinity during scheduling. So, a worker node which already executed a specific function can re-use execution environments with pre-loaded packages.
%
They showed that the cache hit rate was improved, as well as the individual functions execution and turnaround time. Besides, they do not achieve the best load balance results, due to the priority gave to the packages locality instead of the resources availability.

Adressing to the response time minimization challenging, Stein presented an approach based on a noncooperative game-theoretic load balancing, implemented on top of OpenWhisk~\cite{stein2018serverless}.
The concept of noncooperative load balance is that every user, in a distributed scenario, takes into account information regarding service time and allocation from the hosts. With that, they calculate an optimal split of its own perceived arrival rate
in response to other users.
The distributed controller architecture used by OpenWhisk shares common host state information such as the number of concurrently active invocation on each host.
%
Combining the concept of noncooperative load balacing and the architecture of OpenWhisk, Stein presents a noncooperative on-line allocation heuristic (NOAH), where each function contains an expected average waiting time to be handled by an event.
For each one it estimates the required number of instances to respect such average waiting time. The dispatcher first searches for idle instances to schedule
the functions and to get the minimum completion time possible,
otherwise it balances the request based on the functions allocation.
When a function is allocated to a site, its instances are not direclty spawned.
They behave to an instance pool that manage the requests and take the decisions locally.

Suresh et. Gandhi also used OpenWhisk to implement a scheduling policy, named FnSched, 
focused on costs reduction. Their main ideia is that they try to use as less resources - named invokers - as possible ~\cite{suresh2019fnsched}.
For that, they developed and combined two algorithms, the cpu-shares regulation and the greedy one. Called cpu-shares regulation algorithm, it regulates how much cpu the instances will use. They define a latency ratio that measures the quality of the service and verify it over time. If an instance achieve the latency ratio, it receives more cpu-shares - resources. The greedy algorithm will take care of allocating and scaling up the instances. It checks the available memory of the host, and just if needed, more invoker are used.
%
This way they avoid to use many machines, invokers, and reduce costs.
They point that it also reduce the cold start latency because since they prefer to use
the same invoker as long as possible, they keep the containers warm as consequence.
In addition, to avoid cold start latency, they duplicate the container during an invokation to another invoker even if it will not be used.
This way, whenever scale up is needed, some invokers will have a warm pool of containers.
%
The scheduling policies were implemented on top of OpenWhisk which provides a REST interface to accept requests and provide response to them.
The cpu-shares regulation algorithm belongs to each Invoker and the greedy algorithm belongs to the Controller mechanism.
%They performed experiments to show how an unique invoker behaves with co-locating two and three applications to be executed at the same time, respectively. They focused on the latency degradation as their metric. They showed that FnSched respected the latency ratio, different of OpenWhisk and Linux sharing cpu policies which achieve higher values in several cases. Besides, FnSched do not achieve all the best values, few times the other two achieve better results. Evaluating the multi-invokers perspective, they performs two kind of experiments, with single and multiple applications respectively.
%Regarding experiments with a single application, they compared FnSched with Round Robin and Least Connections schedulers.
%They showed that they achieve the best results in terms of invokers used, but, they did not achieve the best results in terms of latency degradation. In fact, they were in the average, not achieving the best results in any of the experiments. When compared scaling with multiple applications, they reduced the number of invokers used again.

With a different focus, James et. Schien present a model and the results of the implementation of a scheduling algorithm based on low carbon emissions~\cite{James2019ALC}.
%
Similarly, our work-group implemented a scheduling policy based on the temperature of boils used to warm environments and water at homes~\cite{ryax_heat_scheduler}.

%It is important to emphasize here that all the use-cases and solutions showed above were develop on top of containers orchestration. Even with Kubernetes, as the last two use-cases, or with OpenLambda~\cite{openlambda} as the first example.

\subsection{Simulations for Serverless Platforms}
\label{subsec:simulations}

Althought a lot is said about improvement of metrics by evolving the scheduling phase of the functions, one of the main challenges of this subject is the cost to perform and evaluate all the possibilities on in-production platforms or services. Such cost can be understood in several ways such as money, time, resources and many others.
%as money due to the use of such platforms for test and studies; time to complete all the study related to one scheduling policy to another; use of resources that should be available for clients; any many others.
Addressing to these problems, simulation is a key practice and solution. Within simulations one can easily perform and compare different scheduling policies without using any of the above mentioned resources.
Besides, it is needed to have accurate results to be fair with real-world environments, and there are not many tool with such accuracy.

When it is discussed benchmarks or simulations for Cloud and Edge Computing, it is needed a set of well known workloads. In the context of Serverless Computing it is the same \cite{kim_functionbench_2019, kim_practical_2019, yu_characterizing_2020}.
Kim et. Lee~\cite{kim_functionbench_2019, kim_practical_2019} presented a set with micro and application benchmarks for serverless platforms. The micro-benchmarks help to measure the performance
of target resources with functions call, such as matrix multiplication, linpack, iperf3, and etc. The application-benchmarks provide entire application with realistic scenarios, dealing with data-oriented flow and several resources together. Some examples are image/video processing, logistic regression, face detection, word generation and etc.

In this scenario, SimGrid~\cite{casanova:hal-01017319} is a framework that allows the development of simulators to be used to prototype, evaluate and compare system designs, platform configurations and algorithmic approaches.
%
Batsim~\cite{dutot:hal-01333471} is a resources and jobs management system (RJMS) simulator developed on top of SimGrid. It allows the development and study of scheduling algorithms.
The design of Batsim helps researchers to apply and compare different scheduling polices to different platforms due to its decoupled development.
%
Batkube~\cite{batkube} is a Kubernetes Cluster Simulator developed on top of Batsim.
It benefits from Batsim platform and scheduling decoupled implementation,
simulating different platforms and workloads connecting them to different scheduling policies. The first work developed with Batkube uses the default scheduler of Kubernetes.

Mahmoudi et. Khazaei present the SimFaas platform~\cite{mahmoudi_simfaas_2021}. It is an open source serverless platform which allows the study of scheduling policies. The platform deals with some aspects of serverless computing such as function instances states, cold/ warm start-up, auto-scaling and etc.
%
%Their platform was built in five modules. The first one, Utility, provides helper functions for plots and calculations. The second one, SimProcess, simulates a single process, in addition to allow comparisons with analytical models. The FunctionInstance module provides the functionality of the instances. The ServerlessSimulator module is resposible by the the  main simulation. Finally, the ServerlessTemporalSimulator module performs simulations similar to the ServerlessSimulator module but it adds extra functionalities and initial parameters.
%
Different to Batsim that allows the implementation of whole platforms, SimFaas abstraction uses four parameters to characterize a platform: expiration threshold, which they point that is usually constant for public serverless platforms such as AWS, Google Cloud and etc; 
arrival process, that describes the frequency that the jobs arrive; 
warm and cold service process, representing the response time took by the platform to reply cold and war requests respectively.
To characterize a workload they consider the instances arrival rate and the response time for warm and cold requests.
%
%To validate their tool, they performed 3 different experiments comparing with one month of execution in public Serverless platforms. The steady state and the transient analysis focused on the arrival or invocation rate of the instances. This way it is possible to study the availability of the platform and if it comports the number of requests. They showed that the number of instances invoked at the same time tends to be constant. Finally, they analysed the cold start probability against the arrival rate. They performed the simulation for different values of the expiration threshold and investigated its impact in the cold start probability.

Within the simulations, a practice to validate a scheduling policy as generic instead of being usefull only on specific case, is to compare such results with benchmarks. Those are studies of performances of general workloads and platforms.
%
One example is presented by Utiugov et al. with a technique to reduce functions cold start latency based on snapshots~\cite{ustiugov_benchmarking_2021}. They save the current state of a VM in the disk and use it when needed. With that they capture the state of the virtual machine monitor (VMM) and the guest-physical memory contents. It reduces the cold-start latency, in addition to require no main memory during the idle periods.
%
They provide a tool named vHive, an open source framework for serverless experimentation. It is based on Containerd for the containers orchestration and on Firecracker for hypervisioning. 
%
Using vHive, they evaluated the functions from FunctionBench.
%When compared, warm functions had at least half of the processing time of cold functions, showing that the snapshoting technique works. But, since the processing time of the cold started functions were much higher than expected, they investigated it deeply. 
%It was discovered that the unexpected delays happened due to several page fault on the cold start snapshoting process.
%Then, they proposed the record-and-prefetch (REAP) technique, which eliminates the majority of guest memory page faults of subsequent functions invocation, once  the first invocation was already done.
%They showed that REAP resulted in good reductions of the cold start latency when compared with the first results.
%Beside, they did not got better results than the ones for warm functions instance.

% Reduce SimFaas a bit

% Add workloads and Benchmarks
\subsection{Simulations on the Global Continuum of Cloud and Edge Platforms}
\label{subsec:global_continuum}

Going one step back from Serverless Platforms, we need to remain that such platforms still run upon Cloud and Edge infrastructures. Then, from one side we need to focus on the challenges that have been arisen, but from the other side, we need to focus on how to better deal with a global continuum point of view. 

Addressing to the challenge of Cloud and Edge Computing Simulations, our work group developed a simulated Edge Platform developed on top Batsim/SimGrid and we proposed several scheduling policies solutions upon a real use-case~\cite{dasilva2020egdge}. It was used to simulate a real Edge Platform use-case based on Smart Heaters allocated over Smart Buildings. Within the simulation we were able to simulate such infrastructure composed by nodes representing the Smart Buildings and computational resources representing the Smart Heaters.
We developed two-level of scheduling policies that first decide the building that a job should be executed and then to decide in which Smart Heater.
Upon such platform we proposed three different scheduling policies based on data locality and compared them with the default policy applied in our real use-case as a baseline.
To perform the comparisons we used several workloads extracted from the real platform.
In addition, Batsim allows the injection of external events and plugins.
The events can abstract real world behaviors for Edge Platforms such as lost of connection to some nodes or more nodes becoming available. The plugin can add more features to the platform, such as a storage controller plugin that dealt with the data movements.
More details can be found in a Master Report~\cite{dasilva2018master} that depicts the implementation of the simulator and provides more information about the state of the art of Cloud and Edge Platform simulators, scheduling policies and metrics.






