# FaaS Platform experiments on top of OpenWhisk

## Summary

This experiments will follow as:
1. Deploy the plataform
2. Run the experiments
3. Analysis of results
    - Data preprocessing
    - Data analysis

## Deploy the platform

It will: 
- Create the Kubernetes cluster
- Deploy OpenWhisk on top of that
- Deploy an event exporter and a minio instance
- Upload the necessary input files to the minio instace

### Commands

```sh
cd scripts
python3 prepare_openwhisk_cluster.py
```

## Run the experiments

It will:

- Be based on the exp_description_example1 file
- Create all Openwhisk actions
- Invoke all  Openwhisk actions
- Use the event exporter to export the log of Kubernetes (events)
- Export the logs of Openwhisk (activations)
- Copy such output to the anaylisis folder.

### Commands:

**Inputs**: exp.yaml

**Outputs**: activations.txt, events.txt

```sh
cd experiments
python3 run_experiments.py -h
```

## Run the analysis

It will:

- Use the .txt files that come from the experiment phase.
- Process and filter each output file, from Kubernetes and from Openwhisk, and produce two csv files activations.csv (OpenWhisk) and events.csv (Kubernetes)
- Run one different analysis per org-mode notebook file in the notebooks folder.

### Commands

**Inputs**: activations.txt, events.txt (that are produced at the experiment phase)

**Outputs**: activations.csv, events.csv

```sh
cd experiments/analysis
python3 log_analysis.py -h
```

**Inputs**: 
- activations.csv, events.csv (that are produced at the previous step of this phase), 
- cpu.csv, memory.csv (that are produced at the experiment phase)

**Outputs**: plots.png

```sh
cd notebooks
cp ../../experiments/cpu.csv ./grafana/
cp ../../experiments/memory.csv ./grafana/
emacs <notebook_file.org>`` and use C-c C-c commands to execute each cell of the notebook file.
```

## Complete workflow example:

```sh
Deploy OpenWhisk on top of G5K using terraform
```

```sh
cd scripts
python3 prepare_openwhisk_cluster.py
cd ..
```

```sh
cd experiments
python3 run_experiments.py -h
python3 run_experiments.py -r experiment_description/exp_description_example1.yaml
python3 run_experiments.py -m events.txt activations.txt
cp events.txt activations.txt analysis/
```

```sh
cd analysis
python3 log_analysis.py -h
python3 log_analysis.py -i events.txt activations.txt -o events.csv activations.csv
[open a new terminal session and port-foward prometheus, I suggest to use tmux then you can split the current windows in several shells.]
kubectl port-forward svc/monitoring-kube-prometheus-prometheus 3000:9090
python query_csv.py http://localhost:3000 'sum(container_memory_working_set_bytes{namespace="openwhisk", pod=~".*invoker.*"}) by (pod)' '2021-10-03T10:00:00.000Z' '2021-10-03T14:00:00.000Z' '10' > memory.csv
python query_csv.py http://localhost:3000 'sum(node_namespace_pod_container:container_cpu_usage_seconds_total:sum_irate{namespace="openwhisk", pod=~".*invoker.*"}) by (pod)' '2021-10-03T10:00:00.000Z' '2021-10-03T14:00:00.000Z' '10' > cpu.csv
cp memory.csv cpu.csv notebooks/grafana/
```

```sh
cd notebooks
emacs [notebook_file.org] 
[and use C-c C-c commands to execute each cell of the notebook file.]
```