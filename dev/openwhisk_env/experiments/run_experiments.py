import os
import subprocess
import json
import yaml
import sys

def manage_kubernetes_event_logs(output_file):
    # To save the pods list in 'pods_list'
    os.system('kubectl get pods -A > pods_list')

    # To search the event-exporter pod in the 'pods_list'
    with open('pods_list', 'r') as read_obj:
        read_lines = read_obj.readlines()
        event_export_line = [s for s in read_lines if "event-exporter" in s]
        event_export_node_name = [s for s in event_export_line[0].split(" ") if "event-exporter" in s][0]

    # To retrieve the logs from the event-exporter pods
    os.system("kubectl logs " + event_export_node_name + " -n monitoring > " + output_file)

def manage_activations(output_file):
    """
        To get the activation list and save it in 'activation_list'.
        This command returns by default the maximum of 30 activations,
        to show more use '--limit MAX_NUMBER', where MAX_NUMBER <= 200.
    """
    os.system('wsk activation list -i -l 200')
    os.system('wsk activation list -i -l 200 > activation_list')

    # To open the 'activation_list' file and produce a list of activation ids
    file_name = "activation_list"
    with open(file_name, "r") as file_object:
        activations = file_object.readlines()
        activation_ids = []
        for activation in activations[1:]:
            # Remove the "" as result of the split
            activation_info = activation.split(" ")
            while ("" in activation_info):
                activation_info.remove("")
            activation_ids.append(activation_info[2])

    # To retrieve the log of all activations, modify and save them as json files.
    activation_log_full = ''
    for activation_id in activation_ids:
        # To retrieve the log of the activation
        activation_file_name = 'activation_log_' + activation_id + '.json'
        cmd = 'wsk activation get ' + activation_id + ' -i ' #+ '> ' + activation_file_name
        activation_log = subprocess.getoutput(cmd)
        activation_log = activation_log[activation_log.find('{'):].replace('\n','').replace(' ','')
        activation_log_full += activation_log + '\n'

    with open(output_file, 'w') as read_obj_new:
        read_obj_new.write(activation_log_full)

def copy_outputs():
    # To copy the activations.txt and events.txt to the analysis folder
    os.system('cp activations.txt events.txt analysis/')

def create_action(creation_data, function_name_target, function_id):
    # Retrieve args for invokation phase
    arg_list = ""
    args = creation_data.get('args')
    for arg in args:
        arg_list += " -" + arg

    # Retrieve actions for creation phase and create the targeted one
    actions = creation_data.get('actions')
    for action in actions:
        action_name = action.get('name')
        if (action_name == function_name_target):
            action_source = action.get('source')
            docker_image = action.get('docker_image')
            if docker_image == None:
                cmd = "wsk action create " + action_name + "_" + str(function_id) + " " + action_source + arg_list
            else:
                cmd = "wsk action create " + action_name + "_" + str(function_id) + " --docker " + docker_image + " " + action_source + arg_list
            print(cmd)
            os.system(cmd)

            # Update all functions timout as much as possible
            cmd = "wsk action update " + action_name + "_" +  str(function_id) + " -t 300000 -i"
            print(cmd)
            os.system(cmd)
    return

def run_exp(exp_description):
    # Read the exp_description.yaml
    with open(exp_description) as file:
        documents = yaml.full_load(file)

    print("Read", documents)

    # Retrieve both phases
    phases = documents.get('exp')
    print("Phases", phases)
    for phase in phases:
        if phase.get('phase') == 'create':
            creation_phase = phase
        if phase.get('phase') == 'invokation':
            invokation_phase = phase

    # Retrieve args for invokation phase
    arg_list = ""
    args = invokation_phase.get('args')
    for arg in args:
        arg_list += " -" + arg

    # Retrieve actions for invokation phase and execute each one of them
    actions = invokation_phase.get('actions')
    for action in actions:
        number_of_executions = 0 # Controls the number of all executions per action
        action_name = action.get('name')
        executions = action.get('executions')

        # For each execution group inside each action
        for execution in executions:
            number_of_repetitions = int(execution.get('repetitions'))
            parameter_list = ""
            parameters = execution.get('parameters')
            for parameter, value in parameters.items():
                parameter_list += " -p " + parameter + ' ' + str(value)

            metadata = str(execution.get('metadata')).replace("\'", "\"")
            parameter_list += " -p metadata '" + metadata + "'"

            # For each execution given the number of repetitions
            repetition = 0
            while repetition < number_of_repetitions:
                function_id = number_of_executions + repetition
                create_action(creation_phase, action_name , function_id)

                cmd_invokation = "wsk action invoke " + action_name + "_" + str(function_id) + parameter_list + arg_list
                print("Command: ", cmd_invokation)
                print("Repetition: ", repetition)
                cmd_result = subprocess.getoutput(cmd_invokation)

                # Wait for the invokation to finish
                activation_id = cmd_result.split(" ")[-1]
                cmd_activation = "wsk activation result "  + activation_id + " -i"
                cmd_result = subprocess.getoutput(cmd_activation).split(" ")[0]
                print("Command result: ", cmd_result)
                while(cmd_result == "error:"):
                    os.system('sleep 1m')
                    cmd_result = subprocess.getoutput(cmd_activation).split(" ")[0]

                repetition += 1

                # Enforce a delay of 1m between each function
                print("Waiting 1m before starting the next invokation")
                os.system('sleep 1m')

            # Update the number of execution total by action
            number_of_executions = number_of_executions + repetition - 1


def print_parameters():
    str = "\nPlease, use one of the following parameters: \n\
    -h                 | help \n\
    -r <exp_file.yaml> | run the experiment described in exp_file.yaml\n\
    -m <events_output_file.txt> <activations_output_file.txt> | manage the logs of kubernetes and openwhisk.\n"

    print(str)
    return

def main():

    argvs = sys.argv
    if (len(argvs) == 1):
        print_parameters()
        return

    else:
        if(argvs[1] == "-h"):
           print_parameters()
        elif(argvs[1] == "-r"):
            if (len(argvs) < 3):
                print_parameters()
            else:
                print("Creating and invoking the actions for exp: " + str(argvs[2]))
                run_exp(argvs[2])
        elif(argvs[1] == "-m"):
            if (len(argvs) < 4):
                print_parameters()

            #print('Getting the Kubernetes event logs')
            #manage_kubernetes_event_logs(argvs[2])

            print('Getting the log of activations')
            manage_activations(argvs[3])

            #print('Sending data to the analysis step')
            #copy_outputs()

main()
