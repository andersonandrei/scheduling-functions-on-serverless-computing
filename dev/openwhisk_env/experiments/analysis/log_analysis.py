import csv
import json
import os
import sys

def read_openwhisk_activations(input_file):
    functions_list = []
    with open(input_file, "r") as file_object:
        events = file_object.readlines()
        event_entries = []
        for event in events:
            try:
                item = json.loads(event)
            except:
                pass
            status_code = item.get("statusCode")
            if (int(status_code) == 0):
                object_uid = item.get("activationId")
                object_name = item.get("name")
                function_name = item.get("name")
                results = item.get("response").get("result")
                #{{a:1, a:2}, {b:1}}
                for result_type in results.keys():
                    #{a:1, a:2}
                    for result in results[result_type].keys():
                        reason = ""
                        metadata_reason = ""
                        node = ""
                        message = ""
                        duration = ""
                        timestamp = ""
                        metadata = ""
                        if(result_type == "latencies"):
                            duration = results[result_type][result]
                            reason = result
                        if(result_type == "timestamps"):
                            timestamp = results[result_type][result]
                            reason = result
                        if(result_type == "metadata"):
                            metadata = results[result_type][result]
                            metadata_reason = "metadata_" + result
                        event_entry = [object_uid, object_name, function_name, duration, timestamp, reason, metadata, metadata_reason, node, message]    
                        event_entries.append(event_entry)
                functions_list.append(function_name)
    return event_entries, functions_list

def read_event_exporter_logs(input_file):
    with open(input_file, "r") as file_object:
        events = file_object.readlines()
        event_entries = []
        for event in events:
            try:
                item = json.loads(event)
            except:
                pass
            #if (item.get("involvedObject") != None):
            #    print(item)
            #print(item)
            reason = item.get("reason")
            creation_timestamp = item.get("metadata").get("creationTimestamp")
            reasons_list = ['Scheduled', 'Pulling', 'Pulled', 'Created', 'Started', 'Killing']
            if(reason in reasons_list):
                #print(item)
                object_uid = item.get("involvedObject").get("uid")
                object_name = item.get("involvedObject").get("name")
                function_name = object_name.split("-")[-2] + "_" + object_name.split("-")[-1]#+ '-' + object_uid.split("-")[-1][-4:]
                #print(function_name)
                first_timestamp = item.get("firstTimestamp")
                last_timestamp = item.get("lastTimestamp")
                first_timestamp = item.get("firstTimestamp")
                message = item.get("message")
                #reason = item.get("reason")
                if (reason == "Scheduled"):
                    # The Scheduled reason does not show the node in the field "Node" because it wasn't there at this time.
                    node = message.split(" ")[-1]
                else:
                    node = item.get("source").get("host")
                event_entry = [object_uid, object_name, function_name, creation_timestamp, first_timestamp, last_timestamp, reason, node, message]
                event_entries.append(event_entry)
    return event_entries

def write_events_as_csv(events, origin, output_file):
    if (origin == 'event_exposer'):
        with open(output_file, "w", newline="") as f:
            writer = csv.writer(f)
            writer.writerow(["uid", "name", "function_name", "creation_timestamp", "first_timestamp", "last_timestamp", "reason", "node", "message"])
            writer.writerows(events)

    if (origin == 'openwhisk_activations'):
        with open(output_file, "w", newline="") as f:
            writer = csv.writer(f)
            writer.writerow(["object_uid", "object_name", "function_name", "duration", "timestamp", "reason", "metadata_value", "metadata_description", "node", "message"])
            writer.writerows(events)

def copy_outputs(events_output_file, activations_output_file):
    # To copy the activations.csv and events.csv to the notebooks folder
    os.system('cp ' + events_output_file + ' ' + activations_output_file + ' ' + './notebooks/')

def print_parameters():
    str = "\nPlease, use one of the following parameters: \n\
    -h                 | help \n\
    -i <events_input_file.txt> <activations_input_file.txt> -o <events_output_file.txt> <activations_output_file.txt> | process the logs of kubernetes and openwhisk. Please follow the order of the parameters.\n"

    print(str)
    return

def main():
    print ('Number of arguments:', len(sys.argv), 'arguments.')
    print ('Argument List:', str(sys.argv))

    argvs = sys.argv
    if (len(argvs) == 2 or  len(argvs) < 7):
        print_parameters()
        return

    else:
        if(argvs[1] == "-h"):
           print_parameters()
        elif(argvs[1] == "-i" and argvs[4] == "-o"):
            activation_entries, actions_list = read_openwhisk_activations(argvs[3])
            print(actions_list)
            write_events_as_csv(activation_entries, 'openwhisk_activations', argvs[6])
            
            event_entries = read_event_exporter_logs(argvs[2])
            #print("Events:", event_entries[1])
            #print("Events:", event_entries[1].split('-')[:-2])
            #print("Activations:", actions_list)
            event_entries = [x for x in event_entries if x[2] in actions_list]
            write_events_as_csv(event_entries, 'event_exposer', argvs[5])

    copy_outputs(argvs[5], argvs[6])

    """
    file_name = 'activations.txt'
    activation_entries, actions_list = read_openwhisk_activations(file_name)
    print(actions_list)
    write_events_as_csv(activation_entries, 'openwhisk_activations')

    file_name = 'events.txt'
    event_entries = read_event_exporter_logs(file_name)
    event_entries = [x for x in event_entries if x[2] in actions_list]
    write_events_as_csv(event_entries, 'event_exposer')
    """

main()