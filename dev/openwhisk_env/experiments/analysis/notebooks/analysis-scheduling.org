#+STARTUP: overview indent inlineimages logdrawer
#+TITLE: Analysis of Scheduling
#+AUTHOR: Andrei
#+LANGUAGE:    en
#+TAGS: noexport(n) Stats(S)
#+TAGS: Teaching(T) R(R) OrgMode(O) Python(P)
#+EXPORT_SELECT_TAGS: Blog
#+OPTIONS:   H:3 num:t toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+COLUMNS: %25ITEM %TODO %3PRIORITY %TAGS
#+SEQ_TODO: TODO(t!) STARTED(s!) WAITING(w@) APPT(a!) | DONE(d!) CANCELLED(c!) DEFERRED(f!)

* LaTeX Configuration                                              :noexport:
  :latex_headers:
  #+LATEX_HEADER: \usepackage{blindtext}
  #+LATEX_HEADER: \usepackage{listings}
  :end:
* Import Libraries
#+begin_src R :results output :session *R* :eval no-export :exports results
library(ggplot2)
library(tidyr)
library(dplyr)
library(scales)
library(stringr)
library(lubridate)
#+end_src

#+RESULTS:
#+begin_example

Attaching package: ‘dplyr’

The following objects are masked from ‘package:stats’:

    filter, lag

The following objects are masked from ‘package:base’:

    intersect, setdiff, setequal, union

Attaching package: ‘lubridate’

The following objects are masked from ‘package:base’:

    date, intersect, setdiff, union
#+end_example

* Read files
#+begin_src R :results output :session *R* :eval no-export :exports results
df_log <- read.csv(file = 'events.csv', sep = ',', stringsAsFactors = FALSE)
#df_log$creation_timestamp <- as.POSIXct(df_log$creation_timestamp, origin="1970-01-01")
df_log$creation_timestamp <- as.POSIXct(df_log$creation_timestamp,format='%Y-%m-%dT%H:%M:%SZ', tzone = "UTC")
df_log$timestamp <- ymd_hms(df_log$creation_timestamp) + hours(2)
df_log$timestamp[is.na(df_log$timestamp)] <- Inf

df_log <- df_log %>%
  #arrange(creation_timestamp) %>%
  group_by(uid) %>%
  mutate(min_timestamp = min(timestamp)) %>%
  ungroup()
summary(df_log)
str(df_log)
head(df_log)

df_activations <- read.csv(file = 'activations.csv', sep = ',', stringsAsFactors = FALSE)
df_activations$timestamp <- ymd_hms(as.POSIXct(df_activations$timestamp, origin="1970-01-01"))
summary(df_activations)
str(df_activations)
#+end_src

#+RESULTS:
#+begin_example
     uid                name           function_name
 Length:96          Length:96          Length:96
 Class :character   Class :character   Class :character
 Mode  :character   Mode  :character   Mode  :character



 creation_timestamp            first_timestamp    last_timestamp
 Min.   :2021-10-03 10:13:32   Length:96          Length:96
 1st Qu.:2021-10-03 10:15:08   Class :character   Class :character
 Median :2021-10-03 10:17:22   Mode  :character   Mode  :character
 Mean   :2021-10-03 10:16:37
 3rd Qu.:2021-10-03 10:17:40
 Max.   :2021-10-03 10:19:31
    reason              node             message
 Length:96          Length:96          Length:96
 Class :character   Class :character   Class :character
 Mode  :character   Mode  :character   Mode  :character



   timestamp                   min_timestamp
 Min.   :2021-10-03 12:13:32   Min.   :2021-10-03 12:13:32
 1st Qu.:2021-10-03 12:15:08   1st Qu.:2021-10-03 12:14:18
 Median :2021-10-03 12:17:22   Median :2021-10-03 12:17:21
 Mean   :2021-10-03 12:16:37   Mean   :2021-10-03 12:16:13
 3rd Qu.:2021-10-03 12:17:40   3rd Qu.:2021-10-03 12:17:26
 Max.   :2021-10-03 12:19:31   Max.   :2021-10-03 12:19:29
tibble [96 × 11] (S3: tbl_df/tbl/data.frame)
 $ uid               : chr [1:96] "2e6094fb-8df3-437d-9276-e9604def1412" "f8f7bdab-3f29-46f8-aa64-020f613e294e" "2e6094fb-8df3-437d-9276-e9604def1412" "2f344e24-2fed-4eab-b2d4-070f112df79c" ...
 $ name              : chr [1:96] "wskow-invoker-00-4-guest-imageprocessing-1" "wskow-invoker-00-5-guest-imageprocessing-2" "wskow-invoker-00-4-guest-imageprocessing-1" "wskow-invoker-00-6-guest-imageprocessing-3" ...
 $ function_name     : chr [1:96] "imageprocessing_1" "imageprocessing_2" "imageprocessing_1" "imageprocessing_3" ...
 $ creation_timestamp: POSIXct[1:96], format: "2021-10-03 10:13:32" "2021-10-03 10:13:32" ...
 $ first_timestamp   : chr [1:96] "" "" "2021-10-03T10:13:33Z" "" ...
 $ last_timestamp    : chr [1:96] "" "" "2021-10-03T10:13:33Z" "" ...
 $ reason            : chr [1:96] "Scheduled" "Scheduled" "Pulling" "Scheduled" ...
 $ node              : chr [1:96] "paravance-21.rennes.grid5000.fr" "paravance-18.rennes.grid5000.fr" "paravance-21.rennes.grid5000.fr" "paravance-12.rennes.grid5000.fr" ...
 $ message           : chr [1:96] "Successfully assigned openwhisk/wskow-invoker-00-4-guest-imageprocessing-1 to paravance-21.rennes.grid5000.fr" "Successfully assigned openwhisk/wskow-invoker-00-5-guest-imageprocessing-2 to paravance-18.rennes.grid5000.fr" "Pulling image \"andersonandrei/python3action:image_processing\"" "Successfully assigned openwhisk/wskow-invoker-00-6-guest-imageprocessing-3 to paravance-12.rennes.grid5000.fr" ...
 $ timestamp         : POSIXct[1:96], format: "2021-10-03 12:13:32" "2021-10-03 12:13:32" ...
 $ min_timestamp     : POSIXct[1:96], format: "2021-10-03 12:13:32" "2021-10-03 12:13:32" ...
[90m# A tibble: 6 x 11[39m
  uid    name   function_name creation_timestamp  first_timestamp last_timestamp
  [3m[90m<chr>[39m[23m  [3m[90m<chr>[39m[23m  [3m[90m<chr>[39m[23m         [3m[90m<dttm>[39m[23m              [3m[90m<chr>[39m[23m           [3m[90m<chr>[39m[23m
[90m1[39m 2e609… wskow… imageprocess… 2021-10-03 [90m10:13:32[39m [90m"[39m[90m"[39m              [90m"[39m[90m"[39m
[90m2[39m f8f7b… wskow… imageprocess… 2021-10-03 [90m10:13:32[39m [90m"[39m[90m"[39m              [90m"[39m[90m"[39m
[90m3[39m 2e609… wskow… imageprocess… 2021-10-03 [90m10:13:33[39m [90m"[39m2021-10-03T10… [90m"[39m2021-10-03T1…
[90m4[39m 2f344… wskow… imageprocess… 2021-10-03 [90m10:13:33[39m [90m"[39m[90m"[39m              [90m"[39m[90m"[39m
[90m5[39m f8f7b… wskow… imageprocess… 2021-10-03 [90m10:13:33[39m [90m"[39m2021-10-03T10… [90m"[39m2021-10-03T1…
[90m6[39m a1ad2… wskow… videoprocess… 2021-10-03 [90m10:13:34[39m [90m"[39m[90m"[39m              [90m"[39m[90m"[39m
[90m# … with 5 more variables: reason <chr>, node <chr>, message <chr>,[39m
[90m#   timestamp <dttm>, min_timestamp <dttm>[39m
  object_uid        object_name        function_name         duration
 Length:62          Length:62          Length:62          Min.   :  0.06837
 Class :character   Class :character   Class :character   1st Qu.:  0.24371
 Mode  :character   Mode  :character   Mode  :character   Median :  8.72075
                                                          Mean   : 70.79732
                                                          3rd Qu.:144.47424
                                                          Max.   :208.93596
                                                          NA's   :36
   timestamp                      reason          metadata_value
 Min.   :2021-10-03 12:17:23   Length:62          Length:62
 1st Qu.:2021-10-03 12:17:29   Class :character   Class :character
 Median :2021-10-03 12:19:12   Mode  :character   Mode  :character
 Mean   :2021-10-03 12:19:16
 3rd Qu.:2021-10-03 12:20:00
 Max.   :2021-10-03 12:23:02
 NA's   :38
 metadata_description   node         message
 Length:62            Mode:logical   Mode:logical
 Class :character     NA's:62        NA's:62
 Mode  :character
'data.frame':	62 obs. of  10 variables:
 $ object_uid          : chr  "7243fa74ca7b46ed83fa74ca7b76ed0c" "7243fa74ca7b46ed83fa74ca7b76ed0c" "7243fa74ca7b46ed83fa74ca7b76ed0c" "7243fa74ca7b46ed83fa74ca7b76ed0c" ...
 $ object_name         : chr  "matmul_5" "matmul_5" "matmul_5" "matmul_5" ...
 $ function_name       : chr  "matmul_5" "matmul_5" "matmul_5" "matmul_5" ...
 $ duration            : num  209 NA NA NA 205 ...
 $ timestamp           : POSIXct, format: NA NA ...
 $ reason              : chr  "function_execution" "" "finishing_time" "starting_time" ...
 $ metadata_value      : chr  "" "4000" "" "" ...
 $ metadata_description: chr  "" "metadata_input" "" "" ...
 $ node                : logi  NA NA NA NA NA NA ...
 $ message             : logi  NA NA NA NA NA NA ...
#+end_example

* Scheduling Analysis
** Filter Data and process data
*** Events
**** Filter the events file
#+begin_src R :results output :session *R* :eval no-export :exports results
df_filtered <- df_log %>%
    filter(
      reason == 'Scheduled' |
      reason == 'Pulling' |
      reason == 'Pulled' |
      reason == 'Created' |
      reason == 'Started' |
      reason == 'Killing')

df_selected <- df_filtered %>%
    select(-name, -uid, -message, -min_timestamp, -first_timestamp, -last_timestamp, -creation_timestamp)
summary(df_selected)
#+end_src

#+RESULTS:
#+begin_example
 function_name         reason              node
 Length:96          Length:96          Length:96
 Class :character   Class :character   Class :character
 Mode  :character   Mode  :character   Mode  :character



   timestamp
 Min.   :2021-10-03 12:13:32
 1st Qu.:2021-10-03 12:15:08
 Median :2021-10-03 12:17:22
 Mean   :2021-10-03 12:16:37
 3rd Qu.:2021-10-03 12:17:40
 Max.   :2021-10-03 12:19:31
#+end_example

**** Process the events durations
#+begin_src R :results output :session *R* :eval no-export :exports results
df_spread <- df_selected %>%
    group_by_at(vars(-timestamp)) %>%
    mutate(sample_id = row_number()) %>% ungroup() %>%
    spread(reason, timestamp) %>%
    select(-sample_id) #%>%

print(df_spread)
df_processed <- df_spread
#+end_src

#+RESULTS:
#+begin_example
[90m# A tibble: 25 x 8[39m
   function_name    node                 Created             Killing
   [3m[90m<chr>[39m[23m            [3m[90m<chr>[39m[23m                [3m[90m<dttm>[39m[23m              [3m[90m<dttm>[39m[23m
[90m 1[39m imageprocessing… paravance-21.rennes… 2021-10-03 [90m12:17:21[39m 2021-10-03 [90m12:19:29[39m
[90m 2[39m imageprocessing… paravance-21.rennes… [31mNA[39m                  2021-10-03 [90m12:19:29[39m
[90m 3[39m imageprocessing… paravance-12.rennes… 2021-10-03 [90m12:17:22[39m 2021-10-03 [90m12:18:11[39m
[90m 4[39m imageprocessing… paravance-12.rennes… [31mNA[39m                  2021-10-03 [90m12:18:11[39m
[90m 5[39m imageprocessing… paravance-18.rennes… [31mNA[39m                  [31mNA[39m
[90m 6[39m imageprocessing… paravance-12.rennes… [31mNA[39m                  [31mNA[39m
[90m 7[39m imageprocessing… paravance-21.rennes… 2021-10-03 [90m12:17:24[39m [31mNA[39m
[90m 8[39m matmul_1         paravance-12.rennes… 2021-10-03 [90m12:17:30[39m [31mNA[39m
[90m 9[39m matmul_1         paravance-21.rennes… [31mNA[39m                  [31mNA[39m
[90m10[39m matmul_2         paravance-12.rennes… 2021-10-03 [90m12:18:13[39m [31mNA[39m
[90m# … with 15 more rows, and 4 more variables: Pulled <dttm>, Pulling <dttm>,[39m
[90m#   Scheduled <dttm>, Started <dttm>[39m
#+end_example

*** Activations
**** Filter the activations file
#+begin_src R :results output :session *R* :eval no-export :exports results
df_activations_selected <- df_activations %>%
    select(
        function_name,
        timestamp,
        reason,
        node,
        object_uid) %>%
    drop_na(timestamp)
        #mutate(duration = (duration / 1000) %% 60)
summary(df_activations_selected)
#+end_src

#+RESULTS:
#+begin_example
 function_name        timestamp                      reason
 Length:24          Min.   :2021-10-03 12:17:23   Length:24
 Class :character   1st Qu.:2021-10-03 12:17:29   Class :character
 Mode  :character   Median :2021-10-03 12:19:12   Mode  :character
                    Mean   :2021-10-03 12:19:16
                    3rd Qu.:2021-10-03 12:20:00
                    Max.   :2021-10-03 12:23:02
   node          object_uid
 Mode:logical   Length:24
 NA's:24        Class :character
                Mode  :character
#+end_example
**** Spread the timestamps of activations by categories

#+begin_src R :results output :session *R* :eval no-export :exports results
df_activations_selected <- df_activations %>%
    select(
       function_name,
       timestamp,
       reason) %>%
    drop_na()
    #mutate(duration = (duration / 1000) %% 60)
summary(df_activations_selected)

df_spread_activations <- df_activations_selected %>%
   group_by_at(vars(-timestamp)) %>%
   mutate(sample_id = row_number()) %>% ungroup() %>%
   spread(reason, timestamp) %>%
   select(-sample_id)

head(df_spread_activations)
tail(df_spread_activations)
#+end_src

#+RESULTS:
#+begin_example
 function_name        timestamp                      reason
 Length:24          Min.   :2021-10-03 12:17:23   Length:24
 Class :character   1st Qu.:2021-10-03 12:17:29   Class :character
 Mode  :character   Median :2021-10-03 12:19:12   Mode  :character
                    Mean   :2021-10-03 12:19:16
                    3rd Qu.:2021-10-03 12:20:00
                    Max.   :2021-10-03 12:23:02
[90m# A tibble: 6 x 3[39m
  function_name     finishing_time      starting_time
  [3m[90m<chr>[39m[23m             [3m[90m<dttm>[39m[23m              [3m[90m<dttm>[39m[23m
[90m1[39m imageprocessing_1 2021-10-03 [90m12:19:28[39m 2021-10-03 [90m12:17:23[39m
[90m2[39m imageprocessing_2 2021-10-03 [90m12:18:10[39m 2021-10-03 [90m12:17:24[39m
[90m3[39m imageprocessing_3 2021-10-03 [90m12:19:44[39m 2021-10-03 [90m12:17:27[39m
[90m4[39m matmul_1          2021-10-03 [90m12:20:56[39m 2021-10-03 [90m12:17:32[39m
[90m5[39m matmul_2          2021-10-03 [90m12:21:41[39m 2021-10-03 [90m12:18:14[39m
[90m6[39m matmul_3          2021-10-03 [90m12:22:41[39m 2021-10-03 [90m12:19:13[39m
[90m# A tibble: 6 x 3[39m
  function_name     finishing_time      starting_time
  [3m[90m<chr>[39m[23m             [3m[90m<dttm>[39m[23m              [3m[90m<dttm>[39m[23m
[90m1[39m matmul_4          2021-10-03 [90m12:22:39[39m 2021-10-03 [90m12:19:14[39m
[90m2[39m matmul_5          2021-10-03 [90m12:23:02[39m 2021-10-03 [90m12:19:32[39m
[90m3[39m videoprocessing_1 2021-10-03 [90m12:20:00[39m 2021-10-03 [90m12:17:28[39m
[90m4[39m videoprocessing_2 2021-10-03 [90m12:20:00[39m 2021-10-03 [90m12:17:29[39m
[90m5[39m videoprocessing_3 2021-10-03 [90m12:19:10[39m 2021-10-03 [90m12:17:29[39m
[90m6[39m videoprocessing_4 2021-10-03 [90m12:19:11[39m 2021-10-03 [90m12:17:30[39m
#+end_example

** Merge the activations and events data

# That occurs some error with imageprocessing2 from Kubernetes side, it was not registered its node.
# df_spread_activations <- df_spread_activations %>% filter(function_name != 'imageprocessing2')

#+begin_src R :results output :session *R* :eval no-export :exports results
df_events_to_join <- df_processed #%>% mutate(id = row_number())
head(df_events_to_join)
df_activations_to_join <- df_spread_activations #%>% mutate(id = row_number())
head(df_activations_to_join)

df_all <- full_join(
   df_events_to_join,
   df_activations_to_join,
   by="function_name") #%>%
   #distinct()
#df_all[is.na(df_all)] = 0
summary(df_all)

df_gathered <- df_all %>%
    gather(
        factor_key = 'TRUE',
        key = 'reason',
        value = 'timestamp',
        -function_name, -node)

df_gathered$reason <- str_replace_all(df_gathered$reason, 'Scheduled', '00_pod_scheduled')
df_gathered$reason <- str_replace_all(df_gathered$reason, 'Pulling', '01_container_pulling')
df_gathered$reason <- str_replace_all(df_gathered$reason, 'Pulled', '02_container_pulled')
df_gathered$reason <- str_replace_all(df_gathered$reason, 'Created', '03_container_created')
df_gathered$reason <- str_replace_all(df_gathered$reason, 'Started', '04_container_started')
df_gathered$reason <- str_replace_all(df_gathered$reason, 'starting_time', '05_function_started')
df_gathered$reason <- str_replace_all(df_gathered$reason, 'finishing_time', '06_function_finished')
df_gathered$reason <- str_replace_all(df_gathered$reason, 'Killing', '07_finishing_container')

head(df_gathered)
summary(df_gathered)
#+end_src

#+RESULTS:
#+begin_example
[90m# A tibble: 6 x 8[39m
  function_name    node                  Created             Killing
  [3m[90m<chr>[39m[23m            [3m[90m<chr>[39m[23m                 [3m[90m<dttm>[39m[23m              [3m[90m<dttm>[39m[23m
[90m1[39m imageprocessing… paravance-21.rennes.… 2021-10-03 [90m12:17:21[39m 2021-10-03 [90m12:19:29[39m
[90m2[39m imageprocessing… paravance-21.rennes.… [31mNA[39m                  2021-10-03 [90m12:19:29[39m
[90m3[39m imageprocessing… paravance-12.rennes.… 2021-10-03 [90m12:17:22[39m 2021-10-03 [90m12:18:11[39m
[90m4[39m imageprocessing… paravance-12.rennes.… [31mNA[39m                  2021-10-03 [90m12:18:11[39m
[90m5[39m imageprocessing… paravance-18.rennes.… [31mNA[39m                  [31mNA[39m
[90m6[39m imageprocessing… paravance-12.rennes.… [31mNA[39m                  [31mNA[39m
[90m# … with 4 more variables: Pulled <dttm>, Pulling <dttm>, Scheduled <dttm>,[39m
[90m#   Started <dttm>[39m
[90m# A tibble: 6 x 3[39m
  function_name     finishing_time      starting_time
  [3m[90m<chr>[39m[23m             [3m[90m<dttm>[39m[23m              [3m[90m<dttm>[39m[23m
[90m1[39m imageprocessing_1 2021-10-03 [90m12:19:28[39m 2021-10-03 [90m12:17:23[39m
[90m2[39m imageprocessing_2 2021-10-03 [90m12:18:10[39m 2021-10-03 [90m12:17:24[39m
[90m3[39m imageprocessing_3 2021-10-03 [90m12:19:44[39m 2021-10-03 [90m12:17:27[39m
[90m4[39m matmul_1          2021-10-03 [90m12:20:56[39m 2021-10-03 [90m12:17:32[39m
[90m5[39m matmul_2          2021-10-03 [90m12:21:41[39m 2021-10-03 [90m12:18:14[39m
[90m6[39m matmul_3          2021-10-03 [90m12:22:41[39m 2021-10-03 [90m12:19:13[39m
 function_name          node              Created
 Length:25          Length:25          Min.   :2021-10-03 12:15:42
 Class :character   Class :character   1st Qu.:2021-10-03 12:16:58
 Mode  :character   Mode  :character   Median :2021-10-03 12:17:26
                                       Mean   :2021-10-03 12:17:24
                                       3rd Qu.:2021-10-03 12:17:40
                                       Max.   :2021-10-03 12:19:31
                                       NA's   :9
    Killing                        Pulled
 Min.   :2021-10-03 12:18:11   Min.   :2021-10-03 12:15:10
 1st Qu.:2021-10-03 12:18:55   1st Qu.:2021-10-03 12:15:17
 Median :2021-10-03 12:19:10   Median :2021-10-03 12:16:25
 Mean   :2021-10-03 12:19:00   Mean   :2021-10-03 12:16:38
 3rd Qu.:2021-10-03 12:19:15   3rd Qu.:2021-10-03 12:17:26
 Max.   :2021-10-03 12:19:29   Max.   :2021-10-03 12:19:30
 NA's   :17                    NA's   :1
    Pulling                      Scheduled
 Min.   :2021-10-03 12:13:33   Min.   :2021-10-03 12:13:32
 1st Qu.:2021-10-03 12:13:35   1st Qu.:2021-10-03 12:13:36
 Median :2021-10-03 12:13:40   Median :2021-10-03 12:15:56
 Mean   :2021-10-03 12:14:04   Mean   :2021-10-03 12:15:54
 3rd Qu.:2021-10-03 12:14:49   3rd Qu.:2021-10-03 12:17:25
 Max.   :2021-10-03 12:15:04   Max.   :2021-10-03 12:19:29
 NA's   :13                    NA's   :1
    Started                    finishing_time
 Min.   :2021-10-03 12:17:21   Min.   :2021-10-03 12:18:10
 1st Qu.:2021-10-03 12:17:25   1st Qu.:2021-10-03 12:19:11
 Median :2021-10-03 12:17:27   Median :2021-10-03 12:20:00
 Mean   :2021-10-03 12:17:57   Mean   :2021-10-03 12:20:27
 3rd Qu.:2021-10-03 12:18:27   3rd Qu.:2021-10-03 12:21:41
 Max.   :2021-10-03 12:19:31   Max.   :2021-10-03 12:23:02
 NA's   :13
 starting_time
 Min.   :2021-10-03 12:17:23
 1st Qu.:2021-10-03 12:17:27
 Median :2021-10-03 12:17:29
 Mean   :2021-10-03 12:17:58
 3rd Qu.:2021-10-03 12:18:14
 Max.   :2021-10-03 12:19:32
[90m# A tibble: 6 x 4[39m
  function_name    node                     reason           timestamp
  [3m[90m<chr>[39m[23m            [3m[90m<chr>[39m[23m                    [3m[90m<chr>[39m[23m            [3m[90m<dttm>[39m[23m
[90m1[39m imageprocessing… paravance-21.rennes.gri… 03_container_cr… 2021-10-03 [90m12:17:21[39m
[90m2[39m imageprocessing… paravance-21.rennes.gri… 03_container_cr… [31mNA[39m
[90m3[39m imageprocessing… paravance-12.rennes.gri… 03_container_cr… 2021-10-03 [90m12:17:22[39m
[90m4[39m imageprocessing… paravance-12.rennes.gri… 03_container_cr… [31mNA[39m
[90m5[39m imageprocessing… paravance-18.rennes.gri… 03_container_cr… [31mNA[39m
[90m6[39m imageprocessing… paravance-12.rennes.gri… 03_container_cr… [31mNA[39m
 function_name          node              reason
 Length:200         Length:200         Length:200
 Class :character   Class :character   Class :character
 Mode  :character   Mode  :character   Mode  :character




   timestamp
 Min.   :2021-10-03 12:13:32
 1st Qu.:2021-10-03 12:15:42
 Median :2021-10-03 12:17:27
 Mean   :2021-10-03 12:17:30
 3rd Qu.:2021-10-03 12:19:11
 Max.   :2021-10-03 12:23:02
 NA's   :54
#+end_example

** Plot the merged data
#+begin_src R :results output :session *R* :eval no-export :exports results
df_plot <- df_gathered %>% drop_na(timestamp)
summary(df_plot)

ggplot(data = df_plot, aes(x = timestamp,
                           y = function_name,
                           color = reason,
                           shape = node)) +
                           #group = function_name)) + #, color = node)) +
    #geom_line(size = 1, position = position_dodge(width = 0.5)) +
    geom_point(position = position_dodge(width = 0.5)) +
    #coord_flip() +
    labs(x = "nodes", y = "timestamp", color = "reason") +
       scale_fill_brewer(palette = "Dark2") +
       theme_bw() +
       theme(legend.position="right") +
       ggsave('full_line_plot_phases.png')

ggplot(data = df_plot, aes(x = node,
                           y = timestamp,
                           color = function_name,
                           shape = reason,
                           group = function_name)) + #, color = node)) +
    geom_line(size = 1, position = position_dodge(width = 0.5)) +
    geom_point(position = position_dodge(width = 0.5)) +
    coord_flip() +
    labs(x = "nodes", y = "timestamp", color = "reason") +
       scale_fill_brewer(palette = "Dark2") +
       theme_bw() +
       theme(legend.position="bottom") +
    guides(colour=guide_legend(nrow=4,byrow=TRUE, title.position = "top"),
           shape=guide_legend(nrow=4,byrow=TRUE, title.position = "top")) +
    ggsave('full_line_plot_phases_nodes.png')

ggplot(data = df_plot, aes(x = timestamp,
                           y = function_name,
                           color = reason,
                           group = function_name)) + #, color = node)) +
    geom_line(size = 1, position = position_dodge(width = 0.7)) +
    geom_point(size = 0.5, position = position_dodge(width = 0.7)) +
    facet_grid(node ~ ., scale="free_y") +
    labs(x = "timestamp", y = "timestamp", color = "reason") +
    scale_fill_brewer(palette = "Dark2") +
    theme_bw() +
    theme(legend.position="bottom") +
    guides(colour=guide_legend(nrow=4,byrow=TRUE, title.position = "top")) +
    ggsave('full_line_plot_phases_facets.png')
#+end_src

#+RESULTS:
#+begin_example
 function_name          node              reason
 Length:146         Length:146         Length:146
 Class :character   Class :character   Class :character
 Mode  :character   Mode  :character   Mode  :character



   timestamp
 Min.   :2021-10-03 12:13:32
 1st Qu.:2021-10-03 12:15:42
 Median :2021-10-03 12:17:27
 Mean   :2021-10-03 12:17:30
 3rd Qu.:2021-10-03 12:19:11
 Max.   :2021-10-03 12:23:02
Saving 7 x 7 in image
Saving 6.99 x 6.99 in image
Warning messages:
1: The shape palette can deal with a maximum of 6 discrete values because
more than 6 becomes difficult to discriminate; you have 8. Consider
specifying shapes manually if you must have them.
2: Removed 33 rows containing missing values (geom_point).
3: The shape palette can deal with a maximum of 6 discrete values because
more than 6 becomes difficult to discriminate; you have 8. Consider
specifying shapes manually if you must have them.
4: Removed 33 rows containing missing values (geom_point).
Saving 6.99 x 6.99 in image
#+end_example
