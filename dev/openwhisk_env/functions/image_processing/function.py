import boto3
import uuid
from time import time
from PIL import Image

#import ops

FILE_NAME_INDEX = 1

from PIL import Image, ImageFilter

TMP = "./"


def flip(image, file_name):
    path_list = []
    path = TMP + "flip-left-right-" + file_name
    img = image.transpose(Image.FLIP_LEFT_RIGHT)
    img.save(path)
    path_list.append(path)

    path = TMP + "flip-top-bottom-" + file_name
    img = image.transpose(Image.FLIP_TOP_BOTTOM)
    img.save(path)
    path_list.append(path)

    return path_list


def rotate(image, file_name):
    path_list = []
    path = TMP + "rotate-90-" + file_name
    img = image.transpose(Image.ROTATE_90)
    img.save(path)
    path_list.append(path)

    path = TMP + "rotate-180-" + file_name
    img = image.transpose(Image.ROTATE_180)
    img.save(path)
    path_list.append(path)

    path = TMP + "rotate-270-" + file_name
    img = image.transpose(Image.ROTATE_270)
    img.save(path)
    path_list.append(path)

    return path_list


def filter(image, file_name):
    path_list = []
    path = TMP + "blur-" + file_name
    img = image.filter(ImageFilter.BLUR)
    img.save(path)
    path_list.append(path)

    path = TMP + "contour-" + file_name
    img = image.filter(ImageFilter.CONTOUR)
    img.save(path)
    path_list.append(path)

    path = TMP + "sharpen-" + file_name
    img = image.filter(ImageFilter.SHARPEN)
    img.save(path)
    path_list.append(path)

    return path_list


def gray_scale(image, file_name):
    path = TMP + "gray-scale-" + file_name
    img = image.convert('L')
    img.save(path)
    return [path]


def resize(image, file_name):
    path = TMP + "resized-" + file_name
    image.thumbnail((128, 128))
    image.save(path)
    return [path]


def image_processing(file_name, image_path):
    path_list = []
    start = time()
    with Image.open(image_path) as image:
        tmp = image
        #path_list = ""
        
        path_list += flip(image, file_name)
        
        """
        path_list += ops.flip(image, file_name)
        path_list += ops.rotate(image, file_name)
        path_list += ops.filter(image, file_name)
        path_list += ops.gray_scale(image, file_name)
        path_list += ops.resize(image, file_name)
        """

    latency = time() - start
    print("PATH_LIST", path_list)
    return latency, path_list


def main():
    print("Começando")

    latencies = {}
    timestamps = {}
    timestamps["starting_time"] = time()

    input_bucket = "input" #event['input_bucket']
    object_key = "SampleImage40mb.jpg" #event['object_key']
    output_bucket = "output" #event['output_bucket']
    endpoint_url = "http://minio.default:9000" #event['endpoint_url']
    aws_access_key_id = "andrei-access" #event['aws_access_key_id']
    aws_secret_access_key = "andrei-secret" #event['aws_secret_access_key']
    metadata = '{"input": "SampleImage_40mb"}' #event['metadata']

    s3_client = boto3.client('s3',
                    endpoint_url=endpoint_url,
                    aws_access_key_id=aws_access_key_id,
                    aws_secret_access_key=aws_secret_access_key)#,                                                                                                                                                                                                                                                                                                            
                    #config=Config(signature_version='s3v4'),                                                                                                                                                                                                                                                                                                                 
                    #region_name='us-east-1')
    start = time()
    download_path = '/tmp/{}{}'.format(uuid.uuid4(), object_key)
    s3_client.download_file(input_bucket, object_key, download_path)
    download_latency = time() - start
    latencies["download_data"] = download_latency
    timestamps["finishing_time"] = time()
    print("Fez o download")
    
    image_processing_latency, path_list = image_processing(object_key, download_path)
    
    latencies["function_execution"] = image_processing_latency
    print("PATH_LIST OUTSIDE", path_list)
    print("processou a imagem")
    

    start = time()
    for upload_path in path_list:
        s3_client.upload_file(upload_path, output_bucket, upload_path.split("/")[FILE_NAME_INDEX])
    upload_latency = time() - start
    latencies["upload_data"] = upload_latency
    timestamps["finishing_time"] = time()
    

    return {"latencies": latencies, "timestamps": timestamps, "metadata": metadata}

main()