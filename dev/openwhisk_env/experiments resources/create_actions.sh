#!/bin/bash

#wsk action create hello1 ../../functions/hello/function.py -i
#wsk action create hello2 ../../functions/hello/function.py -i

wsk action create floatoperation1 ../../functions/float_operation/function.py -i
wsk action create floatoperation2 ../../functions/float_operation/function.py -i

wsk action create linpack1 --docker andersonandrei/python3action:linpack ../../functions/linpack/function.py -i
wsk action create linpack2 --docker andersonandrei/python3action:linpack ../../functions/linpack/function.py -i

wsk action create matmul1 --docker andersonandrei/python3action:matmul ../../functions/matmul/function.py -i
wsk action create matmul2 --docker andersonandrei/python3action:matmul ../../functions/matmul/function.py -i

wsk action create chameleon1 --docker andersonandrei/python3action:chameleon ../../functions/chameleon/function.py -i
wsk action create chameleon2 --docker andersonandrei/python3action:chameleon ../../functions/chameleon/function.py -i

wsk action create videoprocessing1 --docker andersonandrei/python3action:video_processing ../../functions/video_processing/function.py -i
wsk action create videoprocessing2 --docker andersonandrei/python3action:video_processing ../../functions/video_processing/function.py -i

wsk action create imageprocessing1 --docker andersonandrei/python3action:image_processing ../../functions/image_processing/function.py -i
wsk action create imageprocessing2 --docker andersonandrei/python3action:image_processing ../../functions/image_processing/function.py -i

wsk action create modeltraining1 --docker andersonandrei/python3action:model_training ../../functions/model_training/function.py -i
wsk action create modeltraining2 --docker andersonandrei/python3action:model_training ../../functions/model_training/function.py -i

wsk action create pyaes1 --docker andersonandrei/python3action:pyaes ../../functions/pyaes/function.py -i
wsk action create pyaes2 --docker andersonandrei/python3action:pyaes ../../functions/pyaes/function.py -i

wsk action create facedetection1 --docker andersonandrei/python3action:ml_video_face_detection ../../functions/model_serving/ml_video_face_detection/function.py -i
wsk action update facedetection1 -t 300000 -i
wsk action create facedetection2 --docker andersonandrei/python3action:ml_video_face_detection ../../functions/model_serving/ml_video_face_detection/function.py -i
wsk action update facedetection2 -t 300000 -i

wsk action create rnngenerate1 --docker andersonandrei/python3action:rnn_generate_character_level ../../functions/model_serving/rnn_generate_character_level/function.py -i
wsk action create rnngenerate2 --docker andersonandrei/python3action:rnn_generate_character_level ../../functions/model_serving/rnn_generate_character_level/function.py -i