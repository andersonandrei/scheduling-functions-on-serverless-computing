import csv
import json

def read_openwhisk_activations(file_name):
    with open(file_name, "r") as file_object:
        activations = file_object.readlines()
        activation_entries = []
        for activation in activations:

            # Remove the "" as result of the split
            activation_info = activation.split(" ")
            while ("" in activation_info):
                activation_info.remove("")
            
            # Merge datetime
            activation_info[1] = activation_info[0] + 'T' + activation_info[1] + 'Z'
            activation_info = activation_info[1:]
            
            # Merge status fields (positions 5 and 6) and shift the list one position to the left
            if (len(activation_info) == 8):
                activation_info[5] = activation_info[5] + '_' + activation_info[6]
                activation_info[6] = activation_info[7]
                activation_info = activation_info[:7]

            activation_entries.append(activation_info)
    return activation_entries[1:]

def read_event_exporter_logs(file_name):
    with open(file_name, "r") as file_object:
        events = file_object.readlines()
        event_entries = []
        for event in events:
            try:
                item = json.loads(event)
            except:
                pass
            object_uid = item.get("involvedObject").get("uid")
            object_name = item.get("involvedObject").get("name")
            function_name = object_name.split("-")[-1] + '-' + object_uid.split("-")[-1][-4:]
            first_timestamp = item.get("firstTimestamp")
            last_timestamp = item.get("lastTimestamp")
            first_timestamp = item.get("firstTimestamp")
            message = item.get("message")
            reason = item.get("reason")
            node = item.get("source").get("host")
            event_entry = [object_uid, object_name, function_name, first_timestamp, last_timestamp, reason, node, message]
            event_entries.append(event_entry)
    return event_entries

def write_events_as_csv(events, origin):
    if (origin == 'event_exposer'):
        with open("events.csv", "w", newline="") as f:
            writer = csv.writer(f)
            writer.writerow(["uid", "name", "function_name", "first_timestamp", "last_timestamp", "reason", "node", "message"])
            writer.writerows(events)

    if (origin == 'openwhisk_activations'):
        with open("activations.csv", "w", newline="") as f:
            writer = csv.writer(f)
            writer.writerow(["datetime", "activation_id", "kind", "start", "duration", "status", "entity"])
            writer.writerows(events)

def main():
    file_name = 'events.txt'
    event_entries = read_event_exporter_logs(file_name)
    write_events_as_csv(event_entries, 'event_exposer')
    
    file_name = 'activations.txt'
    activation_entries = read_openwhisk_activations(file_name)
    write_events_as_csv(activation_entries, 'openwhisk_activations')

main()