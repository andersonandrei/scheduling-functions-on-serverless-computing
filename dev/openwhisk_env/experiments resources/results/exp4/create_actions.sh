#!/bin/bash

wsk action create hello1 ../functions/hello/function.py -i
wsk action create hello2 ../functions/hello/function.py -i

wsk action create float-operation1 ../functions/float_operation/function.py -i
wsk action create float-operation2 ../functions/float_operation/function.py -i

wsk action create linpack1 --docker andersonandrei/python3action:ml-libs ../functions/linpack/function.py -i
wsk action create linpack2 --docker andersonandrei/python3action:ml-libs ../functions/linpack/function.py -i

wsk action create matmul1 --docker andersonandrei/python3action:ml-libs ../functions/matmul/function.py -i
wsk action create matmul2 --docker andersonandrei/python3action:ml-libs ../functions/matmul/function.py -i
