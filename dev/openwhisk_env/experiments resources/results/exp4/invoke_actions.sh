#!/bin/bash
wsk action invoke hello1 --result --param name World -i
wsk action invoke hello2 --result --param name PHYSICS -i

wsk action invoke float-operation1 --result --param n 10000000 -i
wsk action invoke float-operation2 --result --param n 10000000 -i

wsk action invoke linpack1 --result --param n 5000 -i
wsk action invoke linpack2 --result --param n 5000 -i

wsk action invoke matmul1 --result --param n 2000 -i
wsk action invoke matmul2 --result --param n 2000 -i
