#!/bin/bash

#wsk action create chameleon ../functions/chameleon/function.py -i
#wsk action create float_operation ../functions/float_operation/function.py -i
#wsk action create hello ../functions/hello/function.py -i
#wsk action create image_processing ../functions/image_processing/function.py -i
#wsk action create jokes ../functions/jokes/function.py -i
#wsk action create video_processing ../functions/video_processing/function.py -i
#wsk action create linpack --docker andersonandrei/python3action:ml-libs ../functions/linpack/function.py -i
wsk action create matmul0 --docker andersonandrei/python3action:ml-libs ../functions/matmul/function.py -i
wsk action create matmul1 --docker andersonandrei/python3action:ml-libs ../functions/matmul/function.py -i
wsk action create matmul2 --docker andersonandrei/python3action:ml-libs ../functions/matmul/function.py -i
wsk action create matmul3 --docker andersonandrei/python3action:ml-libs ../functions/matmul/function.py -i