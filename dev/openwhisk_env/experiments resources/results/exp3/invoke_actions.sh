#!/bin/bash

#wsk action invoke hello --result --param name World -i
#wsk action invoke hello --result --param name World -i
#wsk action invoke hello --result --param name Andrei -i

#wsk action invoke chameleon --result --param num_of_rows 100 --param num_of_cols 100 -i
#wsk action invoke chameleon --result --param num_of_rows 500 --param num_of_cols 500 -i
#wsk action invoke chameleon --result --param num_of_rows 1000 --param num_of_cols 1000 -i

#wsk action invoke float_operation --result --param n 100 -i
#wsk action invoke float_operation --result --param n 1000 -i
#wsk action invoke float_operation --result --param n 10000 -i
#wsk action invoke float_operation --result --param n 1000000 -i
#wsk action invoke float_operation --result --param n 10000000 -i
#wsk action invoke float_operation --result --param n 100000000 -i

#wsk action invoke linpack --result --param n 5000 -i
#wsk action invoke linpack --result --param n 5000 -i
#wsk action invoke linpack --result --param n 5000 -i
#wsk action invoke linpack --result --param n 5000 -i
#wsk action invoke linpack --result --param n 10000 -i

wsk action invoke matmul0 --result --param n 2000 -i
wsk action invoke matmul1 --result --param n 2000 -i
wsk action invoke matmul2 --result --param n 2000 -i
wsk action invoke matmul3 --result --param n 2000 -i
