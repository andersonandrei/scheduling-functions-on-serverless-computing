import os
import subprocess
import json
import yaml
import sys

def execution_with_logs(input_list, output_name):
    # To execute the shell command 'cmd' and save the output in the 'output_file'
    my_cmd = input_list
    with open(output_name, 'w') as output_file:
        subprocess.run(my_cmd, stdout=output_file)

def manage_kubernetes_event_logs(output_file):
    # To save the pods list in 'pods_list'
    os.system('kubectl get pods -A > pods_list')

    # To search the event-exporter pod in the 'pods_list'
    with open('pods_list', 'r') as read_obj:
        read_lines = read_obj.readlines()
        event_export_line = [s for s in read_lines if "event-exporter" in s]
        event_export_node_name = [s for s in event_export_line[0].split(" ") if "event-exporter" in s][0]

    # To retrieve the logs from the event-exporter pods
    os.system("kubectl logs " + event_export_node_name + " -n monitoring > " + output_file)

def manage_activations(output_file):
    """ 
        To get the activation list and save it in 'activation_list'.
        This command returns by default the maximum of 30 activations, 
        to show more use '--limit MAX_NUMBER', where MAX_NUMBER <= 200.
    """
    os.system('wsk activation list -i -l 200')
    os.system('wsk activation list -i -l 200 > activation_list')

    # To open the 'activation_list' file and produce a list of activation ids
    file_name = "activation_list"
    with open(file_name, "r") as file_object:
        activations = file_object.readlines()
        activation_ids = []
        for activation in activations[1:]:
            # Remove the "" as result of the split
            activation_info = activation.split(" ")
            while ("" in activation_info):
                activation_info.remove("")
            activation_ids.append(activation_info[2])
    
    # To retrieve the log of all activations, modify and save them as json files.
    activation_log_full = ''
    for activation_id in activation_ids:
        # To retrieve the log of the activation
        activation_file_name = 'activation_log_' + activation_id + '.json'
        cmd = 'wsk activation get ' + activation_id + ' -i ' #+ '> ' + activation_file_name
        activation_log = subprocess.getoutput(cmd)
        activation_log = activation_log[activation_log.find('{'):].replace('\n','').replace(' ','')
        activation_log_full += activation_log + '\n'

    with open(output_file, 'w') as read_obj_new:
        read_obj_new.write(activation_log_full)

"""
def manage_resource_usage(start_date, end_date, step, mem_output_file, cpu_output_file):
    # wait until Openwhisk get ready"
    os.system("sleep 2m")

    commands = [
        # memory
        "python query_csv.py http://localhost:3000 'sum(container_memory_working_set_bytes{namespace='openwhisk', pod=~'.*invoker.*''}) by (pod)' " + start_date + " " + end_date + " " + step + " > " + mem_output_file
        
        #cpu
        "python query_csv.py http://localhost:3000 'sum(node_namespace_pod_container:container_cpu_usage_seconds_total:sum_irate{namespace=+ openwhisk", pod=~'.*invoker.*'}) by (pod)' + start_date + " " + end_date + " " + step + " > " + cpu_output_file
    ]

    os.system("killall kubectl")
    for cmd in commands:
        os.system("kubectl port-forward service/minio 9000 &")
        os.system("sleep 1")
        os.system(cmd)
        os.system("sleep 1")
        os.system("killall kubectl")
"""

def copy_outputs():
    # To copy the activations.txt and events.txt to the analysis folder
    os.system('cp activations.txt events.txt analysis/')

def create_action(creation_data, function_name_target, function_id):
    # Retrieve args for invokation phase
    arg_list = ""
    args = creation_data.get('args')
    for arg in args:
        arg_list += " -" + arg

    # Retrieve actions for creation phase and create the targeted one
    actions = creation_data.get('actions')
    for action in actions:
        action_name = action.get('name')
        if (action_name == function_name_target):
            action_source = action.get('source')
            docker_image = action.get('docker_image')
            if docker_image == None:
                cmd = "wsk action create " + action_name + "_" + str(function_id) + " " + action_source + arg_list    
            else:
                cmd = "wsk action create " + action_name + "_" + str(function_id) + " --docker " + docker_image + " " + action_source + arg_list
            print(cmd)
            os.system(cmd)

            # Update all functions timout as much as possible
            cmd = "wsk action update " + action_name + "_" +  str(function_id) + " -t 300000 -i"
            print(cmd)
            os.system(cmd)
    return

def run_exp(exp_description, delay_between_functions):
    # Read the exp_description.yaml 
    with open(exp_description) as file:
        documents = yaml.full_load(file)
    
    # Retrieve both phases
    phases = documents.get('exp')
    for phase in phases:
        if phase.get('phase') == 'create':
            creation_phase = phase
        if phase.get('phase') == 'invokation':
            invokation_phase = phase
    
    # Retrieve args for invokation phase
    arg_list = ""
    args = invokation_phase.get('args')
    for arg in args:
        arg_list += " -" + arg

    # Retrieve actions for invokation phase and execute each one of them
    actions = invokation_phase.get('actions')
    actions_and_repetitions = {}
    for action in actions:
        #number_of_executions = 0 # Controls the number of all executions per action
        action_name = action.get('name')
        executions = action.get('executions')
        if (actions_and_repetitions.get(action_name) == None):
            actions_and_repetitions[action_name] = 0
        # For each execution group inside each action
        for execution in executions:
            number_of_repetitions = int(execution.get('repetitions'))
            parameter_list = ""
            parameters = execution.get('parameters')
            for parameter, value in parameters.items():
                parameter_list += " -p " + parameter + ' ' + str(value)
            
            metadata = str(execution.get('metadata')).replace("\'", "\"")
            parameter_list += " -p metadata '" + metadata + "'"        
            
            # For each execution given the number of repetitions
            repetitions_done = actions_and_repetitions.get(action_name)
            print('got it', repetitions_done)
            repetition = 1
            while repetition <= number_of_repetitions:
                function_id = repetition + repetitions_done
                create_action(creation_phase, action_name , function_id)

                cmd_invokation = "wsk action invoke " + action_name + "_" + str(function_id) + parameter_list + arg_list
                print("Command: ", cmd_invokation)
                print("Repetition: ", repetition)
                print("function_id: ", function_id)
                cmd_result = subprocess.getoutput(cmd_invokation)
                
                # Wait for the invokation to finish
                activation_id = cmd_result.split(" ")[-1]
                cmd_activation = "wsk activation result "  + activation_id + " -i"
                cmd_result = subprocess.getoutput(cmd_activation).split(" ")[0]
                print("Command result: ", cmd_result)
                while(cmd_result == "error:"):
                    os.system('sleep 5s')
                    cmd_result = subprocess.getoutput(cmd_activation).split(" ")[0]
                
                repetition += 1
                
                # Enforce a delay of 1m between each function
                print("Waiting " + str(delay_between_functions) + " before starting the next invokation")
                os.system('sleep ' + str(delay_between_functions))
            
            # Update the number of execution total by action                
            actions_and_repetitions[action_name] = actions_and_repetitions[action_name] + repetition - 1    
            print('updated', actions_and_repetitions[action_name])     

def print_parameters():
    str = "\nPlease, use one of the following parameters: \n\
    -h                 | help \n\
    -r <exp_file.yaml> <delay_between_invokations> | run the experiment described in exp_file.yaml with the specified delay between the invokations.\n\
    -m <events_output_file.txt> <activations_output_file.txt> | manage the logs of kubernetes and openwhisk.\n"

    print(str)
    return

def main():  
    print ('Number of arguments:', len(sys.argv), 'arguments.')
    print ('Argument List:', str(sys.argv))

    argvs = sys.argv
    if (len(argvs) == 1):
        print_parameters()
        return

    else:
        if(argvs[1] == "-h"):
           print_parameters()
        elif(argvs[1] == "-r"):
            if (len(argvs) < 4):
                print_parameters()
            else:
                print("Creating and invoking the actions for exp: " + str(argvs[2]) + "with delay of: " + str(argvs[3]))
                run_exp(argvs[2], argvs[3])
        elif(argvs[1] == "-m"):
            if (len(argvs) < 4):
                print_parameters()

            print('Getting the Kubernetes event logs')
            manage_kubernetes_event_logs(argvs[2])

            print('Getting the log of activations')
            manage_activations(argvs[3])

            #print('Collecting resource usage')
            #manage_resource_usage(argv[4], argv[5])

            print('Sending data to the analysis step')
            copy_outputs()

    #print("Creating and invoking the actions")
    #run_exp('exp_description.yaml')

    #print('Getting the Kubernetes event logs')
    #manage_kubernetes_event_logs()

    #print('Getting the log of activations')
    #manage_activations()

    #print('Sending data to the analysis step')
    #copy_outputs()

main()

