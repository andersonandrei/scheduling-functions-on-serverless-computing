#!/bin/bash

# Create actions
echo 'Creating actions'
./create_actions.sh

echo 'Invoking actions'
./invoke_actions.sh > invokations_output

echo 'Getting the log of activations'
wsk activations list -i > activations_output

#echo 'Getting the log of the scheduling events'
#kubectl logs event-exporter-6868ffd96-88jhz -n monitoring > analysis/events.txt 
