#!/bin/bash

#wsk action invoke hello1 --result --param name World -i
#wsk action invoke hello2 --result --param name PHYSICS -i
#sleep 2m

wsk action invoke floatoperation1 --result --param n 10000000 -i
wsk action invoke floatoperation2 --result --param n 10000000 -i
#sleep 2m

wsk action invoke linpack1 --result --param n 5000 -i
wsk action invoke linpack2 --result --param n 5000 -i
#sleep 2m

wsk action invoke matmul1 --result --param n 2000 -i
wsk action invoke matmul2 --result --param n 2000 -i
#sleep 2m

wsk action invoke chameleon1 --result -p num_of_rows 100 -p num_of_cols 100 -i
wsk action invoke chameleon2 --result -p num_of_rows 100 -p num_of_cols 100 -i
#sleep 2m

wsk action invoke videoprocessing1 --result -p object_key "SampleVideo_1280x720_10mb.mp4" -p input_bucket "input" -p output_bucket "output" -p endpoint_url "http://minio.default:9000" -p aws_access_key_id "andrei-access" -p aws_secret_access_key "andrei-secret" -i
wsk action invoke videoprocessing2 --result -p object_key "SampleVideo_1280x720_10mb.mp4" -p input_bucket "input" -p output_bucket "output" -p endpoint_url "http://minio.default:9000" -p aws_access_key_id "andrei-access" -p aws_secret_access_key "andrei-secret" -i
#sleep 2m

wsk action invoke imageprocessing1 --result -p object_key "animal-dog.jpg" -p input_bucket "input" -p output_bucket "output" -p endpoint_url "http://minio.default:9000" -p aws_access_key_id "andrei-access" -p aws_secret_access_key "andrei-secret" -i
wsk action invoke imageprocessing2 --result -p object_key "animal-dog.jpg" -p input_bucket "input" -p output_bucket "output" -p endpoint_url "http://minio.default:9000" -p aws_access_key_id "andrei-access" -p aws_secret_access_key "andrei-secret" -i
#sleep 2m

wsk action invoke modeltraining1 --result -p dataset_bucket "input" -p dataset_object_key "reviews50mb.csv" -p model_bucket "output" -p model_object_key "lr_model.pk" -p endpoint_url "http://minio.default:9000" -p aws_access_key_id "andrei-access" -p aws_secret_access_key "andrei-secret" -i
wsk action invoke modeltraining2 --result -p dataset_bucket "input" -p dataset_object_key "reviews50mb.csv" -p model_bucket "output" -p model_object_key "lr_model.pk" -p endpoint_url "http://minio.default:9000" -p aws_access_key_id "andrei-access" -p aws_secret_access_key "andrei-secret" -i
#sleep 2m

wsk action invoke pyaes1 --result -p length_of_message 10 -p num_of_iterations 1 -i
wsk action invoke pyaes2 --result -p length_of_message 10 -p num_of_iterations 1 -i
#sleep 2m

wsk action invoke facedetection1 --result -p object_key "SampleVideo_1280x720_10mb.mp4" -p input_bucket "input" -p output_bucket "output" -p model_bucket "input" -p model_object_key "haarcascade_frontalface_default.xml" -p endpoint_url "http://minio.default:9000" -p aws_access_key_id "andrei-access" -p aws_secret_access_key "andrei-secret" -i
#sleep 2m
wsk action invoke facedetection2 --result -p object_key "SampleVideo_1280x720_10mb.mp4" -p input_bucket "input" -p output_bucket "output" -p model_bucket "input" -p model_object_key "haarcascade_frontalface_default.xml" -p endpoint_url "http://minio.default:9000" -p aws_access_key_id "andrei-access" -p aws_secret_access_key "andrei-secret" -i
#sleep 2m

wsk action invoke rnngenerate1 --result -p language "Portuguese" -p start_letters "ABCDEFGHIJKLMNOP" -p model_bucket "input" -p model_object_key "rnn_model.pth" -p model_parameter_object_key "rnn_params.pkl" -p endpoint_url "http://minio.default:9000" -p aws_access_key_id "andrei-access" -p aws_secret_access_key "andrei-secret" -i
wsk action invoke rnngenerate2 --result -p language "Portuguese" -p start_letters "ABCDEFGHIJKLMNOP" -p model_bucket "input" -p model_object_key "rnn_model.pth" -p model_parameter_object_key "rnn_params.pkl" -p endpoint_url "http://minio.default:9000" -p aws_access_key_id "andrei-access" -p aws_secret_access_key "andrei-secret" -i
#sleep 2m