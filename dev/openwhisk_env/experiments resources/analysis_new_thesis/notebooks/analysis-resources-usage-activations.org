#+STARTUP: overview indent inlineimages logdrawer
#+TITLE: Analysis of Resources Usage
#+AUTHOR: Andrei
#+LANGUAGE:    en
#+TAGS: noexport(n) Stats(S)
#+TAGS: Teaching(T) R(R) OrgMode(O) Python(P)
#+EXPORT_SELECT_TAGS: Blog
#+OPTIONS:   H:3 num:t toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+COLUMNS: %25ITEM %TODO %3PRIORITY %TAGS
#+SEQ_TODO: TODO(t!) STARTED(s!) WAITING(w@) APPT(a!) | DONE(d!) CANCELLED(c!) DEFERRED(f!)

* LaTeX Configuration                                              :noexport:
  :latex_headers:
  #+LATEX_HEADER: \usepackage{blindtext}
  #+LATEX_HEADER: \usepackage{listings}
  :end:
* Import Libraries
#+begin_src R :results output :session *R* :eval no-export :exports results
library(ggplot2)
library(tidyr)
library(dplyr)
library(scales)
library(stringr)
library(purrr)
library(lubridate)
library(formattable)

#options(crayon.enabled = FALSE)
#+end_src

#+RESULTS:
#+begin_example

Attaching package: ‘dplyr’

The following objects are masked from ‘package:stats’:

    filter, lag

The following objects are masked from ‘package:base’:

    intersect, setdiff, setequal, union

Attaching package: ‘purrr’

The following object is masked from ‘package:scales’:

    discard

Attaching package: ‘lubridate’

The following objects are masked from ‘package:base’:

    date, intersect, setdiff, union

Attaching package: ‘formattable’

The following objects are masked from ‘package:scales’:

    comma, percent, scientific
#+end_example
* Define Functions
#+begin_src R :results output :session *R* :eval no-export :exports results
                                        # Read the logs
read_input_files <- function(file_name) {
    df_resources <- read.csv(file = file_name, sep = ',', stringsAsFactors = FALSE)
    #df_resources$time <- ymd_hms(as.POSIXct(df_resources$timestamp, origin = "1970-01-01", tzone = "UTC")) # hours(2)
    df_resources$time <- df_resources$timestamp
    df_resources <- df_resources %>% select(-timestamp, -name)
    df_resources <- df_resources %>% arrange(time)
    return (df_resources)
}

read_activations_results <- function(file_name) {
    df_activations <- read.csv(file = 'activations.csv', sep = ',', stringsAsFactors = FALSE)
    #df_activations$timestamp <- ymd_hms(as.POSIXct(df_activations$timestamp, origin = "1970-01-01", tzone = "UTC")) # hours(2)
    df_activations <- df_activations %>% arrange(timestamp)
    return (df_activations)
}

process_activations <- function(df_activations) {

    df_activations_selected <- df_activations %>%
        select(
            object_uid,
            function_name,
            metadata_value,
            duration,
            timestamp,
            reason) #%>%
    #mutate(duration = (duration / 1000) %% 60)

    # Replicate the metada_value for all entries of the same object_uid
    df_activations_selected <- df_activations_selected %>%
        group_by(object_uid) %>%
        mutate(metadata_value = max(metadata_value)) %>%
        ungroup()

    df_activations_selected <- df_activations_selected %>%
        group_by(function_name, metadata_value) %>%
        mutate(function_metadata = paste(strsplit(function_name, "\\_")[[1]][1],
                                     metadata_value,
                                     sep="_"))

    #df_activations_selected <- df_activations_selected %>%
    #    filter(!str_detect(function_name, "chameleon"))

    return (df_activations_selected)
}

process_resources <- function(df_resources, metric, df_timestamps) {

   # To convert Bytes to MB
    if(metric == 'memory') {
        df_resources$values <- df_resources$values #/ 1024 / 1024
        #print("$$$$$$$$$ Memory")
        #print(summary(df_resources))

        #df_resources <- df_resources %>% filter(values > 1024 * 1024 * 10)
    }

    if(metric == 'cpu') {
        df_resources$values <- df_resources$values #* 100
        #print("$$$$$$$$$ CPU")
        #print(summary(df_resources))

        df_resources <- df_resources #%>% filter(values > 0.2)
    }


    df_resources <- df_resources %>% rename(function_name = pod)

    # Filter the standard OpenWhisk pods
    df_filtered <- df_resources %>%
        filter(!str_detect(function_name, "health")) %>%
        filter(!str_detect(function_name, "nodejs")) %>%
        filter(str_detect(function_name, "wskow")) #%>%
        #filter(!str_detect(function_name, "chameleon"))

    # Mutate the dataframe updating pod's names and adding a function_group column
    df_filtered <- df_filtered %>%
        group_by(function_name) %>%
        mutate(function_name = paste(
                   strsplit(function_name, "\\-")[[1]][6],
                   strsplit(function_name, "\\-")[[1]][7],
                   sep = "_")) %>%
                                        #mutate(function_group = strsplit(function_name, "\\_")[[1]][1]) %>%
        ungroup()

    df_filtered <- df_filtered %>% rename(timestamp_resources = time, usage = values)

    return (df_filtered)
}

select_timestamps <- function(resource_file) {
    # Read cpu resource usage input file to find there the good timestamps
    df_resource <- read_input_files(resource_file)
    df_resource_filtered <- process_resources(df_resource, 'cpu', df_resource)

    df_resource_filtered <- df_resource_filtered %>%
        select(timestamp_resources) %>%
        rename(time=timestamp_resources)

    return (df_resource_filtered)
}

filter_timestamps <- function (df_resources_filtered, df_timestamps) {
    #df_filtered <- df_resources_filtered %>%
    #    group_by(function_name) %>%
    #    filter(timestamp_resources %in% df_timestamps$timestamp_resources) %>%
    #    ungroup()

    df_filtered <- inner_join(df_resources_filtered,
                              df_timestamps,
                              by=c("timestamp_resources", "function_name"))


    return(df_filtered)
}


combine_dataframes <- function(df_activations, df_resources) {

    df_activations <- df_activations %>% select(-duration, -timestamp)

    df_merge <- right_join(
        df_activations,
        df_resources,
        by="function_name") %>%
        distinct()

    df_merge <- df_merge %>%
        group_by(function_name) %>%
        mutate(function_group = strsplit(function_name, "\\_")[[1]][1]) %>%
        ungroup() %>%
        drop_na()#%>%

    return (df_merge)
}

#+end_src

#+RESULTS:

* Define Plots
#+begin_src R :results output :session *R* :eval no-export :exports results

plot_data <- function(df_all, metric, label_text) {

    df_plot <- df_all #%>% filter(usage > 0)
    df_plot$timestamp_resources <- ymd_hms(as.POSIXct(df_plot$timestamp_resources, origin = "1970-01-01", tzone = "UTC")) # hours(2)

    #a <- df_plot %>% filter(function_metadata == 'matmul_3000') #& function_group == 'floatoperation_100000000')
    #ggplot(data = a, aes(x = timestamp_resources, y = usage, color = function_name)) +
    #        geom_point() +
    #        labs(x = "Time", y = label_text, color = "Function_Input") +
    #        scale_fill_brewer(palette = "Dark2") +
    #        theme_bw() +
    #        theme(legend.position="bottom") +
    #        guides(colour=guide_legend(ncol=2,byrow=TRUE, title.position = "top")) +
    #        ggsave(paste('usage-line-andrei', metric, '.png', sep="-"))

    print('ploting data....')

    df_function_group <- as.data.frame(df_plot$function_group) %>% distinct()
    for (i in 1:nrow(df_function_group)) {
        group <- df_function_group[[1]][i]

        df_plot_filtered <- df_plot %>% filter(function_group == group)

        ggplot(data = df_plot_filtered, aes(x = timestamp_resources, y = usage, color = function_metadata)) +
            geom_line() +
            labs(x = "Time", y = label_text, color = "Function_Input") +
            scale_fill_brewer(palette = "Dark2") +
            theme_bw() +
            theme(legend.position="bottom") +
            guides(colour=guide_legend(ncol=2,byrow=TRUE, title.position = "top")) #+
            ggsave(paste('usage-line', group, metric, '.png', sep="-"))

        ggplot(data = df_plot_filtered, aes(x = timestamp_resources, y = usage, color = function_metadata)) +
            geom_point() +
            labs(x = "Time", y = label_text, color = "Function_Input") +
            scale_fill_brewer(palette = "Dark2") +
            theme_bw() +
            theme(legend.position="bottom") +
            guides(colour=guide_legend(ncol=2,byrow=TRUE, title.position = "top")) #+
            ggsave(paste('usage-point', group, metric, '.png', sep="-"))
        }

    print('ploting data2....')

    ggplot(data = df_plot, aes(x = timestamp_resources, y = usage, color = function_group)) +
        geom_line() + #stat='identity', position = position_stack(reverse = TRUE), color = "black")  +
        labs(x = "Time", y = label_text, color = "function_group") +
                                        #facet_grid(function_group ~ .) +
                                        #facet_wrap(function_group ~ .,  ncol=2) +
        scale_fill_brewer(palette = "Dark2") +
        theme_bw() +
        theme(legend.position="bottom") +
        guides(colour=guide_legend(ncol=2,byrow=TRUE, title.position = "top")) +
        ggsave(paste('usage-line-all-', metric, '.png', sep=""))

    ggplot(data = df_plot, aes(x = timestamp_resources, y = usage, color = function_group)) +
        geom_point() + #stat='identity', position = position_stack(reverse = TRUE), color = "black")  +
        labs(x = "Time", y = label_text, color = "function_group") +
                                        #facet_grid(function_group ~ .) +
                                        #facet_wrap(function_group ~ .,  ncol=2) +
        scale_fill_brewer(palette = "Dark2") +
        theme_bw() +
        theme(legend.position="bottom") +
        guides(colour=guide_legend(ncol=2,byrow=TRUE, title.position = "top")) +
        ggsave(paste('usage-point-all-', metric, '.png', sep=""))

    print('ploting data3....')

    ggplot(data = df_plot, aes(x = timestamp_resources, y = usage, color = function_metadata)) +
        geom_line() + #stat='identity', position = position_stack(reverse = TRUE), color = "black")  +
        labs(x = "Time", y = label_text, color = "Function_Input") +
        #facet_grid(function_group ~ .) +
        facet_wrap(function_group ~ .,  ncol=2, scales="free_x") +
        scale_fill_brewer(palette = "Dark2") +
        theme_bw() +
        theme(legend.position="bottom") +
        guides(colour=guide_legend(ncol=2, byrow=TRUE, title.position = "top")) #+
        #ggsave(paste('usage-line-facets-', metric, '.png', sep=""))

    ggplot(data = df_plot, aes(x = timestamp_resources, y = usage, color = function_metadata)) +
        geom_point() + #stat='identity', position = position_stack(reverse = TRUE), color = "black")  +
        labs(x = "Time", y = label_text, color = "Function_Input") +
        #facet_grid(function_group ~ .) +
        facet_wrap(function_group ~ .,  ncol=2, scales="free") +
        scale_fill_brewer(palette = "Dark2") +
        theme_bw() +
        theme(legend.position="right") +
        guides(colour=guide_legend(ncol=1, byrow=TRUE, title.position = "top")) #+
        #ggsave(paste('usage-point-facets-', metric, '.png', sep=""))

    print('ploting data4....')

    ggplot(data = df_plot, aes(x = usage, y = function_metadata, color = function_group)) +
        geom_boxplot() + #stat='identity', position = position_stack(reverse = TRUE), color = "black")  +
        labs(x = label_text, y = "Function") +
        scale_fill_brewer(palette = "Dark2") +
        theme_bw() +
                                        #guides(colour=guide_legend(ncol=4,byrow=TRUE, title.position = "top")) +
        ggsave(paste('usage-boxplot-all-', metric, '.png', sep=""))

    print('ploting data5....')

    ggplot(data = df_plot, aes(x = usage, y = function_metadata)) +
        geom_boxplot() + #stat='identity', position = position_stack(reverse = TRUE), color = "black")  +
        labs(x = label_text, y = "Function Input") +
        facet_wrap(function_group ~ .,  ncol=2, scales="free") +
        scale_fill_brewer(palette = "Dark2") +
        theme_bw() +
        theme(legend.position = "none") +
        ggsave(paste('usage-boxplot-facet-', metric, '.png', sep=""))

    print("Cabou")
}


#+end_src

#+RESULTS:

* Execute Functions
#+begin_src R :results output :session *R* :eval no-export :exports results
file_names <- c('memory.csv',
       'cpu.csv')#,
       #'iops.csv',
       #'received_bandwidth.csv',
       #'transmitted_bandwidth.csv',
       #'throughput.csv')

metrics <- c('memory',
       'cpu')#,
       #'iops',
       #'received_bandwidth',
       #'transmitted_bandwidth',
       #'throughput')

labels <- c('Memory Usage (MB)',
       'Cpu Usage (%)')#,
       #'IOPS',
       #'Received Bandwidth',
       #'Transmitted Bandwidth',
       #'Throughput')

df_resources <- read_input_files('./grafana/cpu.csv')
df_resources_filtered <- process_resources(df_resources, metric)
df_timestamps <- df_resources_filtered %>% select(-usage)
df_input_files <- data.frame(file_names, metrics, labels)

for (i in 1:nrow(df_input_files)){
    file_name <- paste('./grafana/',df_input_files[i, 1], sep="")
    metric <- df_input_files[i, 2]
    label <- df_input_files[i, 3]

    df_activations <- read_activations_results('activations.csv')
    df_activations_filtered <- process_activations(df_activations)

    df_resources <- read_input_files(file_name)
    df_resources_filtered <- process_resources(df_resources, metric)
    df_resources_filtered_timestamps <- filter_timestamps(df_resources_filtered, df_timestamps)

    #write.csv(df_resources_filtered_timestamps, paste("df_resources_filtered", metric, ".csv"), row.names = TRUE)
    df_all <- combine_dataframes(df_activations_filtered, df_resources_filtered_timestamps)

    plot_data(df_all, metric, label)
}
#+end_src

#+RESULTS:
#+begin_example
[1] "ploting data...."
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
[1] "ploting data2...."
Saving 7 x 7 in image
Saving 7 x 7 in image
[1] "ploting data3...."
[1] "ploting data4...."
Saving 7 x 7 in image
[1] "ploting data5...."
Saving 7 x 7 in image
[1] "Cabou"
[1] "ploting data...."
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
Saving 7 x 7 in image
[1] "ploting data2...."
Saving 7 x 7 in image
Saving 7 x 7 in image
[1] "ploting data3...."
[1] "ploting data4...."
Saving 7 x 7 in image
[1] "ploting data5...."
Saving 7 x 7 in image
[1] "Cabou"
#+end_example

* Tests

#+begin_src R :results output :session *R* :eval no-export :exports results
file_names <- c('memory.csv',
       'cpu.csv')#,
       #'iops.csv',
       #'received_bandwidth.csv',
       #'transmitted_bandwidth.csv',
       #'throughput.csv')

metrics <- c('memory',
       'cpu')#,
       #'iops',
       #'received_bandwidth',
       #'transmitted_bandwidth',
       #'throughput')

labels <- c('Memory Usage (MB)',
       'Cpu Usage (%)')#,
       #'IOPS',
       #'Received Bandwidth',
       #'Transmitted Bandwidth',
       #'Throughput')

df_input_files <- data.frame(file_names, metrics, labels)
timestamps <- select_timestamps(df_input_files)
print(summary(timestamps))
#+end_src

#+RESULTS:
#+begin_example
[1] "$$$$$$$$$ CPU"
     values              pod                 time
 Min.   : 0.000000   Length:25797       Min.   :2021-08-31 12:00:00
 1st Qu.: 0.000000   Class :character   1st Qu.:2021-09-01 02:26:00
 Median : 0.000000   Mode  :character   Median :2021-09-01 05:30:50
 Mean   : 0.247757                      Mean   :2021-09-01 04:33:31
 3rd Qu.: 0.000002                      3rd Qu.:2021-09-01 12:04:10
 Max.   :24.165379                      Max.   :2021-09-01 18:25:20
                 Min.               1st Qu.                Median
"2021-08-31 12:00:30" "2021-08-31 15:34:27" "2021-09-01 04:20:55"
                 Mean               3rd Qu.                  Max.
"2021-09-01 03:13:30" "2021-09-01 12:45:22" "2021-09-01 16:37:20"
#+end_example

* Step by step
** Read resources
#+begin_src R :results output :session *R* :eval no-export :exports results                                        # Read the logs

    df_resources <- read.csv(file = file_name, sep = ',', stringsAsFactors = FALSE)
    df_resources$time <- ymd_hms(df_resources$Time)
    df_resources <- df_resources %>% select(-Time)
    df_resources <- df_resources %>% arrange(time)
#+end_src
*** Prepare resources
#+begin_src R :results output :session *R* :eval no-export :exports results
   # Gather the datafram by pod and usage
    df_gathered <- df_resources %>%
        gather(
            factor_key = 'TRUE',
            key = 'function_name',
            value = 'usage',
            -time) %>%
        drop_na()

    df_gathered$function_name <- as.character(df_gathered$function_name)

                                        # To convert Bytes to MB
    if(metric == 'memory') {
        df_gathered$usage <- df_gathered$usage / 1024 / 1024
    }
    if(metric == 'cpu') {
        df_gathered$usage <- df_gathered$usage / 100
    }

    #summary(df_gathered)

    # Filter the standard OpenWhisk pods
    df_filtered <- df_gathered %>%
        filter(!str_detect(function_name, "nodejs")) %>%
        filter(str_detect(function_name, "wskow"))

    # Mutate the dataframe updating pod's names and adding a function_group column
    df_filtered <- df_filtered %>% group_by(function_name) %>%
        mutate(function_name = paste(
                   strsplit(function_name, "\\.")[[1]][6],
                   strsplit(function_name, "\\.")[[1]][7],
                   sep = "_")) %>%
        #mutate(function_group = strsplit(function_name, "\\_")[[1]][1]) %>%
        ungroup()

   df_filtered <- df_filtered %>% rename(timestamp_resources = time)

    # Mutate the dataframe adding an ID to each execution of a function_group
    #df_filtered <- df_filtered %>%
    #    group_split(function_group) %>%
    #   map_df(~.x %>% group_by(function_name) %>%
    #               mutate(id = cur_group_id()))

                                        # More filters for OpenWhisk standard pods
                                        #df_filtered <- df_filtered %>% filter(pod != 'nodejs10' & pod != 'invokerhealthtestaction0')
                                        #str(df_filtered)
                                        #head(df_filtered)
                                        #tail(df_filtered)
#+end_src
** Read activations
#+begin_src R :results output :session *R* :eval no-export :exports results
    df_activations <- read.csv(file = 'activations.csv', sep = ',', stringsAsFactors = FALSE)
    df_activations$timestamp <- ymd_hms(as.POSIXct(df_activations$timestamp, origin = "1970-01-01", tzone = "UTC")) # hours(2)
    df_activations <- df_activations %>% arrange(timestamp)
#+end_src
*** Prepare activations
#+begin_src R :results output :session *R* :eval no-export :exports results
    df_activations_selected <- df_activations %>%
        select(
            function_name,
            duration,
            timestamp,
            reason,
            node,
            object_uid,
            metadata_value,
            metadata_description) #%>%
    #mutate(duration = (duration / 1000) %% 60)

    # Replicate the metada_value for all entries of the same object_uid
    df_activations_selected <- df_activations_selected %>%
        group_by(object_uid) %>%
        mutate(metadata_value = max(metadata_value)) %>%
        mutate(function_metadata = paste(strsplit(function_name, "\\_")[[1]][1],
                                     metadata_value,
                                     sep="_")) %>%
        ungroup()
    df_activations_selected <- df_activations_selected %>% rename(timestamp_activatoins = timestamp)
#+end_src
** Merge Dataframes
#+begin_src R :results output :session *R* :eval no-export :exports results
    df_merge <- full_join(
        df_activations,
        df_resources,
        by="function_name") %>%
        distinct()

    df_merge <- df_merge %>%
        group_by(function_name) %>%
        mutate(function_group = strsplit(function_name, "\\_")[[1]][1]) %>%
        ungroup() #%>%
    #    na.omit()
#+end_src
** Plot them
#+begin_src R :results output :session *R* :eval no-export :exports results
    df_plot <- df_all
    #print(df_plot$function_group)
    print('ploting data....')

    df_function_group <- as.data.frame(df_plot$function_group) %>% distinct()
    group <- df_function_group[[1]][i]

    df_plot_filtered <- df_plot %>% filter(function_group == group)
    print('df_plot_filtered')
    print(summary(df_plot_filtered))

    ggplot(data = df_plot_filtered, aes(x = timestamp_resources, y = usage, color = function_metadata)) +
        geom_line() +
        labs(x = "timestamp", y = label_text, color = "function_metadata") +
        scale_fill_brewer(palette = "Dark2") +
        theme_bw() +
        theme(legend.position="bottom") +
        guides(colour=guide_legend(ncol=4,byrow=TRUE, title.position = "top")) +
        ggsave(paste('usage-line', group, metric, '.png', sep="-"))

    print('ploting data2....')

    ggplot(data = df_plot, aes(x = timestamp_resources, y = usage, color = function_group)) +
        geom_line() + #stat='identity', position = position_stack(reverse = TRUE), color = "black")  +
        labs(x = "timestamp", y = label_text, color = "function_group") +
                                        #facet_grid(function_group ~ .) +
                                        #facet_wrap(function_group ~ .,  ncol=2) +
        scale_fill_brewer(palette = "Dark2") +
        theme_bw() +
        theme(legend.position="bottom") +
        guides(colour=guide_legend(ncol=4,byrow=TRUE, title.position = "top")) +
        ggsave(paste('usage-line-all-', metric, '.png', sep=""))

    print('ploting data3....')

    ggplot(data = df_plot, aes(x = timestamp_resources, y = usage, color = function_metadata)) +
        geom_line() + #stat='identity', position = position_stack(reverse = TRUE), color = "black")  +
        labs(x = "timestamp_resources", y = label_text, color = "function_metadata") +
        #facet_grid(function_group ~ .) +
        facet_wrap(function_group ~ .,  ncol=2) +
        scale_fill_brewer(palette = "Dark2") +
        theme_bw() +
        theme(legend.position="bottom") +
        guides(colour=guide_legend(ncol=4,byrow=TRUE, title.position = "top")) +
        ggsave(paste('usage-line-facets-', metric, '.png', sep=""))

    print('ploting data4....')

    ggplot(data = df_plot, aes(x = usage, y = function_metadata, color = function_group)) +
        geom_boxplot() + #stat='identity', position = position_stack(reverse = TRUE), color = "black")  +
        labs(x = label_text, y = "pods") +
        scale_fill_brewer(palette = "Dark2") +
        theme_bw() +
                                        #guides(colour=guide_legend(ncol=4,byrow=TRUE, title.position = "top")) +
        ggsave(paste('usage-boxplot-all-', metric, '.png', sep=""))

    print('ploting data5....')

    ggplot(data = df_plot, aes(x = function_metadata, y = usage)) +
        geom_boxplot() + #stat='identity', position = position_stack(reverse = TRUE), color = "black")  +
        labs(x = "Function Execution ID", y = label_text) +
        facet_wrap(function_group ~ .,  ncol=2, scales="free_x") +
        scale_fill_brewer(palette = "Dark2") +
        theme_bw() +
        theme(legend.position = "none") +
        ggsave(paste('usage-boxplot-facet-', metric, '.png', sep=""))

    print("Cabou")


#+end_src
