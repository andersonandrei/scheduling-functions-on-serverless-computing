import csv
import json
import os

def read_openwhisk_activations(file_name):
    functions_list = []
    with open(file_name, "r") as file_object:
        events = file_object.readlines()
        event_entries = []
        for event in events:
            try:
                item = json.loads(event)
            except:
                pass
            status_code = item.get("statusCode")
            if (int(status_code) == 0):
                object_uid = item.get("activationId")
                object_name = item.get("name")
                #duration = item.get("duration")
                function_name = item.get("name")
                results = item.get("response").get("result")
                for result in results.keys():
                    reason = result
                    duration = results[result]
                    #reason = "Executed"
                    node = ""
                    message = ""
                    event_entry = [object_uid, object_name, function_name, duration, reason, node, message]
                    event_entries.append(event_entry)
                functions_list.append(function_name)
    return event_entries, functions_list

def read_event_exporter_logs(file_name):
    with open(file_name, "r") as file_object:
        events = file_object.readlines()
        event_entries = []
        for event in events:
            try:
                item = json.loads(event)
            except:
                pass
            #if (item.get("involvedObject") != None):
            #    print(item)
            reason = item.get("reason")
            reasons_list = ['Scheduled', 'Pulling', 'Pulled', 'Created', 'Started', 'Killed']
            if(reason in reasons_list):
                print(item)
                object_uid = item.get("involvedObject").get("uid")
                object_name = item.get("involvedObject").get("name")
                function_name = object_name.split("-")[-1] #+ '-' + object_uid.split("-")[-1][-4:]
                first_timestamp = item.get("firstTimestamp")
                last_timestamp = item.get("lastTimestamp")
                first_timestamp = item.get("firstTimestamp")
                message = item.get("message")
                #reason = item.get("reason")
                node = item.get("source").get("host")
                event_entry = [object_uid, object_name, function_name, first_timestamp, last_timestamp, reason, node, message]
                event_entries.append(event_entry)
    return event_entries

def write_events_as_csv(events, origin):
    if (origin == 'event_exposer'):
        with open("events.csv", "w", newline="") as f:
            writer = csv.writer(f)
            writer.writerow(["uid", "name", "function_name", "first_timestamp", "last_timestamp", "reason", "node", "message"])
            writer.writerows(events)

    if (origin == 'openwhisk_activations'):
        with open("activations.csv", "w", newline="") as f:
            writer = csv.writer(f)
            writer.writerow(["object_uid", "object_name", "function_name", "duration", "reason", "node", "message"])
            writer.writerows(events)

def copy_outputs():
    # To copy the activations.csv and events.csv to the notebooks folder
    os.system('cp activations.csv events.csv ./notebooks/')


def main():
    file_name = 'activations.txt'
    activation_entries, actions_list = read_openwhisk_activations(file_name)
    print(actions_list)
    write_events_as_csv(activation_entries, 'openwhisk_activations')

    file_name = 'events.txt'
    event_entries = read_event_exporter_logs(file_name)
    event_entries = [x for x in event_entries if x[2] in actions_list]
    write_events_as_csv(event_entries, 'event_exposer')

    copy_outputs()

main()