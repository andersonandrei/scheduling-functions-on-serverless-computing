# Modifying Kubernetes Scheduler Policies

In the [Kuberenetes documentation](https://kubernetes.io/docs/reference/scheduling/config/) is possible to find some tutorials that show how we can modify Kubernetes Standard Scheduling Policy just by enabling or disabling some Profiles.

The question is: how to deploy such modifications? Kubernetes also provides a [tutorial](https://kubernetes.io/docs/tasks/extend-kubernetes/configure-multiple-schedulers/) for that, but it lacks few updates.

So, we show in this tutorial how to do such a new deployment, with a few corrections. Please, read them before trying this tutorial. Here you will find modified files that worked in our example, but they have all better explained and detailed.

## Summary

This tutorial will follow as:

0. [Maybe] Compile a scheduler with the same version of your Kubernetes Server. In my case, it was 1.20.11 and 1.20 respectivly. 
    > Skip this step at first, and if the deployment fails, start again from here

1. Create a new [Config Map](https://kubernetes.io/docs/concepts/configuration/configmap/#using-configmaps-as-files-from-a-pod) to allow your new scheduler to follow the new policies.

2. Modify the Kuberenetes Cluster Role: `clusterrole.yaml`

3. Deploy the new, and modified, Scheduler: `my-shceduler.yaml`

4. Create few pods for testing it:
  - `pod1.yaml` asks for the standard scheduler, 
  - `pod2.yaml` asks for the new scheduler, 
  - `pod3.yaml` asks for a scheduler that does not exist (then we have a failing example).

## Step by step

### Create a new Config Map

  Create a new Config Map and save its definition in `kube-extra-scheduler-new-config.yaml`.
  It will generate a generic new Config Map. We will extend it after inside our new Scheduler.

  ```
  kubectl create configmap kube-extra-scheduler --from-file=./kube-extra-scheduler-new-config.yaml --output yaml -n kube-system
  ```

  Then check if it is working:

  ```
  kubectl get cm -A

  ...
  NAMESPACE            NAME                                 DATA   AGE
  default              kube-root-ca.crt                     1      92s
  kube-node-lease      kube-root-ca.crt                     1      92s
  kube-public          cluster-info                         2      105s
  kube-public          kube-root-ca.crt                     1      92s
  kube-system          coredns                              1      105s
  kube-system          extension-apiserver-authentication   6      108s
  kube-system          kube-extra-scheduler                 1      30s
  kube-system          kube-proxy                           2      105s
  kube-system          kube-root-ca.crt                     1      92s
  kube-system          kubeadm-config                       2      106s
  kube-system          kubelet-config-1.20                  1      106s
  local-path-storage   kube-root-ca.crt                     1      92s
  local-path-storage   local-path-config                    1      104s
  ```

### Modify Kuberenetes Cluster Role

  As mentioned before, all explanations are pointed in the links above. Here we are going to copy the configuration from `clusterrole.yaml`.

  ```
  export EDITOR=emacs # Let's use Emacs for that :)

  kubectl edit clusterrole system:kube-scheduler
  ```

  Paste there the cofiguration copied, then save it and you will see:

  ```
  clusterrole.rbac.authorization.k8s.io/system:kube-scheduler edited
  ```

### Create (deploy) the new Scheduler:

  We are ready to deploy the new scheduler `my-scheduler.yaml`. Please, notice that in the last part of the file we are using the Config Map created, as well as defining the policies that we want to use in our scheduler:

  ```
  apiVersion: v1
  data:
    kube-extra-scheduler-new-config.yaml: |-
      apiVersion: kubescheduler.config.k8s.io/v1beta1
      kind: KubeSchedulerConfiguration
      profiles:
        - schedulerName: myscheduler
          plugins:
            score:
              disabled:
                - name: NodeResourcesLeastAllocated
      leaderElection:
        leaderElect: false

  kind: ConfigMap
  metadata:
    name: myscheduler
    namespace: kube-system
  ```

  To create the scheduler:

  ```
  kubectl create -f my-scheduler.yaml
  ```

  Then check if it is working:

  ```
  kubect get pods -A
  ```

  Output:

  ```
  ...
  kube-system          myscheduler-79fb79568d-2lp4s                 0/1     Pending   0     0s                         
  kube-system          myscheduler-79fb79568d-2lp4s                 0/1     Pending   0          0s                         
  kube-system          myscheduler-79fb79568d-2lp4s                 0/1     ContainerCreating   0          0s               
  kube-system          myscheduler-79fb79568d-2lp4s                 0/1     Running             0          6s               
  kube-system          myscheduler-79fb79568d-2lp4s                 1/1     Running             0          14s

  kubectl describe pod -n kube-system myscheduler-79fb79568d-2lp4s

  Events:
    Type    Reason     Age   From               Message
    ----    ------     ----  ----               -------
    Normal  Scheduled  44s   default-scheduler  Successfully assigned kube-system/myscheduler-79fb79568d-2lp4s to kind-control-plane
    Normal  Pulling    43s   kubelet            Pulling image "andersonandrei/my-kube-scheduler:1.20.11"
    Normal  Pulled     39s   kubelet            Successfully pulled image "andersonandrei/my-kube-scheduler:1.20.11" in 4.76529252s
    Normal  Created    39s   kubelet            Created container kube-second-scheduler
    Normal  Started    39s   kubelet            Started container kube-second-scheduler
  ```

### Create (deploy) new pods:

  Please, notice that all the three pods are the same, but they ask for different schedulers, though their `spec` definition:

  - `pod1.yaml`: does not ask for any scheduler (so, it will use the standard one):

    ```
    spec:
    ```
    
  - `pod2.yaml`: asks for the new scheduler, myscheduler:

    ```
    spec:
      schedulerName: myscheduler
    ```

  - `pod3.yaml`: asks for a scheduler that does not exist, thirdscheduler (so, it will not be deployed):

    ```
    spec:
      schedulerName: thirdscheduler
    ```

  Let's create them, and check if they are running, respectivly:

  ```
  kubectl create -f pod1.yaml

  kubect get pods -A

  ...
  default              no-annotation                                1/1     Running   0          3m23s

  kubectl describe pod -n default no-annotation

  ...
  Conditions:
    Type              Status
    Initialized       True 
    Ready             True 
    ContainersReady   True 
    PodScheduled      True 
  Volumes:
    default-token-tvzjr:
      Type:        Secret (a volume populated by a Secret)
      SecretName:  default-token-tvzjr
      Optional:    false
  QoS Class:       BestEffort
  Node-Selectors:  <none>
  Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                  node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
  Events:
    Type    Reason     Age    From               Message
    ----    ------     ----   ----               -------
    Normal  Scheduled  5m57s  default-scheduler  Successfully assigned default/no-annotation to kind-control-plane
    Normal  Pulled     5m57s  kubelet            Container image "k8s.gcr.io/pause:2.0" already present on machine
    Normal  Created    5m57s  kubelet            Created container pod-with-no-annotation-container
    Normal  Started    5m57s  kubelet            Started container pod-with-no-annotation-container
  ```

  ```
  kubectl create -f pod2.yaml

  kubect get pods -A
  ...
  default              annotation-second-scheduler                  1/1     Running   0          6m56s

  kubectl describe pod -n default annotation-second-scheduler
  ...
  Conditions:
    Type              Status
    Initialized       True 
    Ready             True 
    ContainersReady   True 
    PodScheduled      True 
  Volumes:
    default-token-tvzjr:
      Type:        Secret (a volume populated by a Secret)
      SecretName:  default-token-tvzjr
      Optional:    false
  QoS Class:       BestEffort
  Node-Selectors:  <none>
  Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                  node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
  Events:
    Type    Reason     Age   From         Message
    ----    ------     ----  ----         -------
    Normal  Scheduled  10m   myscheduler  Successfully assigned default/annotation-second-scheduler to kind-control-plane
    Normal  Pulling    10m   kubelet      Pulling image "k8s.gcr.io/pause:2.0"
    Normal  Pulled     10m   kubelet      Successfully pulled image "k8s.gcr.io/pause:2.0" in 1.137802787s
    Normal  Created    10m   kubelet      Created container pod-with-second-annotation-container
    Normal  Started    10m   kubelet      Started container pod-with-second-annotation-container
  ```

  ```
  kubectl create -f pod3.yaml

  kubect get pods -A

  ...
  default              annotation-third-scheduler                   0/1     Pending   0          54s

  kubectl describe pod -n default annotation-third-scheduler

  ...
  Events:          <none>
  ```